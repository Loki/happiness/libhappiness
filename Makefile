CC=g++
AR=ar
CFLAGS= -Wall
DEPS = i2cmple.h cap1188.h cap1214.h drv2667.h tca9548a.h mtch6303.h gpio_irq.h button_set.h i2c_interface.h
OBJ = i2cmple.o cap1188.o cap1214.o drv2667.o tca9548a.o mtch6303.o gpio_irq.o button_set.o
TARGETOBJ = libhappiness.a
TEST = test.o
DOC = doxygen

all: $(TARGET)

$(TARGET): $(OBJ)
	$(AR) rcs $@ $^ $(CFLAGS)

%.o:
	$(CC) -c $< $(CFLAGS)

test: $(TESTOBJ) $(OBJ)
	g++ -o $@ $^ $(CFLAGS)

doc:
	$(DOC) doxygen.config
	@$(MAKE) -C doc/latex

clean:
	rm -f *.o
	rm -f test

mrproper: clean
	rm -rf doc/latex
	rm -rf doc/html

.PHONY: clean all doc mrproper
