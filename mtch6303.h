/**
 * @file mtch6303.h
 * @authors Julien Decaudin
 * @brief mtch6303 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once

#include "i2c_interface.h"

//DEBUG
#include <stdio.h>
//DEBUG

#define MTCH6303_ADDR	0x25


class Mtch6303 {
	public:
		enum mtch6303_register_t {
			MTCH6303_TOUCH_STATUS	= 0x00,
			MTCH6303_TOUCH_0		= 0x01
		};

		/**
		 * Structure for mtch6303 touch data.
		 * Represent the touch data
		 */
		struct mtch6303_touch_data_t {
			uint8_t status;
			uint8_t touchId; /**< ???. ??? */
			uint16_t x;
			uint16_t y;
		};

		Mtch6303(I2C* i2cH);
		int read_touch_data(uint8_t *nbTouch, mtch6303_touch_data_t *buff);
	private:
        I2C* i2c;	/**< I2C Communication*/
};
