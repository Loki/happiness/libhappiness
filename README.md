# libhappiness

Library for driving piezo actuators, and sensing capacitive single touches for buttons, and multi-touch for touchpads.

## Licence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

This development was initially funded by the European project [Happiness](http://happiness-project.eu/), H2020 grant #645145.

Contact: Thomas Pietrzak (thomas.pietrzak@univ-lille.fr)

## Supported chips

* [TI dRV2667](http://www.ti.com/product/DRV2667): piezo driver.
* [Microchip CAP1188](http://www.microchip.com/CAP1188): capacitive touch driver.
* [Microchip CAP1214](http://www.microchip.com/CAP1214): capacitive touch driver.
* [Microchip MTCH6303](http://www.microchip.com/MTCH6303): capacitive multi-touch driver.
* [TI TCA9548a](http://www.ti.com/product/TCA9548A): I2C multiplexer.

## Dependencies

The library requires [cmake](https://cmake.org/), and [puredata](https://github.com/pure-data/pure-data) or preferably [purr-data](https://git.purrdata.net/jwilkes/purr-data) (see [Installation for Linux](https://github.com/agraef/purr-data/wiki/Installation#linux
)).

The documentation requires [doxygen](http://www.doxygen.org), [gnuplot](http://www.gnuplot.info/), [graphviz](https://www.graphviz.org/) and [wget](https://www.gnu.org/software/wget/) as well as pdflatex if you want the pdf documentation.

## Compilation

To configure the library:

    mkdir build
    cd build
    cmake ..

To generate the library:

    make

To generate a test example:

    make test

To generate the doc:

    make doc

To install puredata objects:

    sudo make install
