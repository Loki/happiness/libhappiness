/**
 * @file drv2667.cpp
 * @authors Julien Decaudin
 * @brief DRV2667 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <stdio.h>
#include <unistd.h>
#include "drv2667.h"

Drv2667::Drv2667(I2C* i2cH, drv2667_gain_t gain) : i2c(i2cH) {
	//Init registers
	//Exit standby mode
    i2c->write8(DRV2667_ADDR, CONFIG_2, 0x00, 0b01000000);//Set bit 6 from 0x02 to 0
	//if (r < 0) { return r; }//TODO check for error
	//Select gain
    i2c->write8(DRV2667_ADDR, CONFIG_1, gain, 0b00000011);// 100 V (Digital) - 40.7 dB (Analog)
	//if (r < 0) { return r; }//TODO check error
}

int Drv2667::set_gain(drv2667_gain_t gain) {
	return i2c->write8(DRV2667_ADDR, CONFIG_1, gain, 0b00000011);
}

int Drv2667::init_analog() {
	int r;
	//Set interface mode to analog
    r = i2c->write8(DRV2667_ADDR, CONFIG_1, 0x01 << 2, 0b00000100);
	if (r < 0) {return r;}
	//Set override
    r = i2c->write8(DRV2667_ADDR, CONFIG_2, 0x01 << 1, 0b00000010);
	if (r < 0) {return r;}

	return 0;
}

int Drv2667::init_digital(drv2667_timeout_t timeout) {
	int r;
	//Set interface mode to digital
    r = i2c->write8(DRV2667_ADDR, CONFIG_1, 0x00, 0b00000100);
	if (r < 0) { return r; }
	//Set timeout period if using digital
    r = i2c->write8(DRV2667_ADDR, CONFIG_2, timeout << 2, 0b00001100);
	if (r < 0) { return r; }

	return 0;
}

int Drv2667::switch_page(uint8_t pnum) {
    return i2c->write8(DRV2667_ADDR, PAGE_REG, pnum, 0xFF);
}

int Drv2667::set_playlist(uint8_t *list) {
    return i2c->write_bytes(DRV2667_ADDR, WAVEFORM_0, list, 8);
}

int Drv2667::play() {
    return i2c->write8(DRV2667_ADDR, CONFIG_2, 0x01, 0b00000001);
}

int Drv2667::stop() {
    return i2c->write8(DRV2667_ADDR, CONFIG_2, 0x00, 0b00000001);
}

int Drv2667::write_ram_data(drv2667_header_t header, drv2667_waveform_synthesis_t *wavef, uint8_t nbwf) {
	int r;
	r = Drv2667::switch_page(1);//Move to ram page 0
	if (r < 0) { return r; }
	//Set size  (N waveform (5N bytes) + 1 header size (1 byte) -1 = 5N)
    r = i2c->write8(DRV2667_ADDR, 0x00, 5 * header.neffect, 0xFF);
	if (r < 0) { return r; }
	//Write array of effect
    r = i2c->write_bytes(DRV2667_ADDR, 0x01, (uint8_t *)header.effects,
							header.neffect * sizeof(drv2667_effect_t));
	if (r < 0) { return r; }
	//Write waveforms
	r = Drv2667::switch_page(2);//Move to ram page 1
	if (r < 0) { return r; }
    r = i2c->write_bytes(DRV2667_ADDR, 0x00,
							(uint8_t *)wavef, nbwf * sizeof(drv2667_waveform_synthesis_t));
	if (r < 0) { return r; }
	r = Drv2667::switch_page(0);//Go back to control page
	if (r < 0) { return r; }

	return 0;
}
