/**
 * @file i2cmple.h
 * @authors Julien Decaudin
 * @brief I2C communication on Raspbian
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once

#include "i2c_interface.h"

class I2cmple : public I2C {
	private:
		int fd;
	public:
		/**
		 * Init i2c device and return file descriptor.
		 */
		I2cmple();
		~I2cmple();

		int short_read(uint8_t addr, uint8_t *data);

		int read8(uint8_t addr, uint8_t reg, uint8_t *data);

		int read_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb);

		int short_write(uint8_t addr, uint8_t data);

		int write8(uint8_t addr, uint8_t reg, uint8_t data, uint8_t mask);

		int write_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb);

};
