/**
 * @file i2c_interface.h
 * @authors Julien Decaudin
 * @brief interface for I2C communication
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once

#include <stdint.h>

class I2C {
    public:
        virtual ~I2C() {}

		/**
		 * Read 1 byte from the device at addr using a short read.
		 * @param addr	Address of the I2C device.
		 * @param data	Ptr to which value are stored.
		 */
		virtual int short_read(uint8_t addr, uint8_t *data) = 0;

		/**
		 * Read 1 byte from register reg on device at addr.
		 * @param addr	Address of the I2C device.
		 * @param reg	The register address from which byte is read.
		 * @param data	Ptr to which value are stored.
		 */
		virtual int read8(uint8_t addr, uint8_t reg, uint8_t *data) = 0;

		/**
		 * Read multiple bytes from register reg on device at addr.
		 * @param addr	Address of the I2C device.
		 * @param reg	The register address from which bytes are read.
		 * @param data	Ptr to which value are stored.
		 * @param nb Number of byte to be read
		 */
		virtual int read_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb) = 0;

		/**
		 * Issue a short write to the given addr.
		 * @param addr	Address of the I2C device.
		 * @param value	value to write.
		 */
		virtual int short_write(uint8_t addr, uint8_t data) = 0;

		/**
		 * Write 1 byte on a I2C device in a given register using a mask.
		 * @param addr	Address of the I2C device.
		 * @param reg 	The register address to which bytes are being written.
		 * @param data	Byte to write.
		 * @param mask Single byte mask to allow to update only certain bit of the register.
		 */
		virtual int write8(uint8_t addr, uint8_t reg, uint8_t data, uint8_t mask) = 0;

		/**
		 * Write multiple bytes on I2C device starting at a given register.
		 * @param addr	Address of the device.
		 * @param nb 	Number of byte to be written.
		 * @param reg 	The register address to which bytes are being written.
		 * @param data	Ptr to bytes to write.
		 */
		virtual int write_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb) = 0;
};
