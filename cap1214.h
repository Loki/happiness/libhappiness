/**
 * @file cap1214.h
 * @authors Julien Decaudin
 * @brief CAP1214 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once
#include <stdint.h>
#include "i2c_interface.h"
#include "i2cmple.h"

#define CAP1214_ADDR_DEFAULT	0x28

class Cap1214 {
	public:
		enum cap1214_register_t {
			CAP1214_MAIN_CTRL		= 0x00,/**< Main Control.*/
			CAP1214_BTN_STATUS_1	= 0x03,
			CAP1214_BTN_STATUS_2	= 0x04,
			CAP1214_SLIDER_POS		= 0x06,/**< Slider position, Volumetric Data.*/
			CAP1214_VOLUM_STEP		= 0x09,
			CAP1214_DELTA_COUNT		= 0x10,/**< [0x10 -> 0x1D] Sensor Input Delta Count. Stores the delta count for CSx (0x10 = CS1, 0x1D = CS14)*/
			CAP1214_QUEUE_CTRL		= 0x1E,
			CAP1214_DATA_SENSITIV	= 0x1F,/**< Sensitivity Control Controls the sensitivity of the threshold and delta counts and data scaling of the base counts*/
			CAP1214_GLOBAL_CONFIG	= 0x20,
			CAP1214_SENSOR_ENABLE	= 0x21,/**< Sensor Input Enable Controls whether the capacitive touch sensor inputs are sampled*/
			CAP1214_BTN_CONFIG		= 0x22,
			CAP1214_GROUP_CFG_1		= 0x23,
			CAP1214_GROUP_CFG_2		= 0x24,
			CAP1214_FRCE_CALIB		= 0x26,
			CAP1214_IRQ_ENABLE_1	= 0x27,
			CAP1214_IRQ_ENABLE_2	= 0x28,
			CAP1214_SENSOR_THRES	= 0x30,/**< [30h -> 37h] Sensor Input Threshold. Stores the delta count threshold to determine a touch for Capacitive Touch Sensor Input x (30h = 1, 37h = 8)*/
			CAP1214_GLOBAL_CONFIG_2	= 0x40
		};

		/**
		 * Init the cap1214 to use with I2C
		 */
        Cap1214(I2C* i2cH);

		/**
		 * Clear the interrupt bit from the main control register
		 * @param cap Ptr to the cap1214 struct.
		 */
		int clear_interrupt_bit();

		/**
		 * Run the recalibration routine
		 */
		int recalibrate_sensors();

		/**
		 * Set the sensitivity for all 14 sensors.
		 * Specify the sensitivity with a value between 0b000 (most sensitive) and 0b111 (less sensitive)
		 */
		int set_sensor_sensitivity(uint8_t sensitivity);

		/**
		 * Set the threshold value of all 7 sensors + the grouped sensors.
		 * Specify the delta count value above which a touch is detected
		 * @param list Ptr to a list of 8 values representing the threshold.
		 * @note every threshold value should be < 127
		 */
		int set_sensor_threshold(int8_t *list);

		/**
		 * Read the sensors input delta count.
		 * @param cap Ptr to the cap1214 struct
		 * @param buffer Ptr to a int8_t array (size 14)
		 */
		int read_delta_count(int8_t *buffer);

		 /**
		  * Read the estimated position of the finger on the slider.
		  * @param cap Ptr to the cap1214 struct
		  * @param buffer Ptr to a float
		  * @param resolution Resolution wanted for the slider
		  */
		int read_cursor_position(float *buffer, int resolution);
	private:
        I2C* i2c;	/**<I2C communication.*/
		uint8_t addr;
};
