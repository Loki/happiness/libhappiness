#=============================================================================
# Copyright 2017-present Christian Frisson.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================

macro(add_pd_library LIBRARY_NAME)

if(APPLE)
	set(LIB_SUFFIX ".pd_darwin")
	# From pure-data/pd-lib-builder/Makefile.pdlibbuilder
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -undefined suppress -flat_namespace")
elseif(UNIX)
	set(LIB_SUFFIX ".pd_linux")
	# From pure-data/pd-lib-builder/Makefile.pdlibbuilder
	# cflags = -march=armv7-a -mfpu=vfpv3 -mfloat-abi=hard
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -rdynamic -shared -fPIC -Wl,-rpath,\"\$$ORIGIN\",--enable-new-dtags")
	link_libraries("-lc -lm")
elseif(WIN32 OR WIN64 OR MINGW)
	set(LIB_SUFFIX ".dll")
else()
	message(FATAL_ERROR "Unsupported platform for AddPureDataLibrary.cmake")
endif()

set(SRCS "${ARGN}")
#foreach(SRC IN LISTS SRCS)
#	message("SRC: ${SRC}")
#endforeach()

add_library(${LIBRARY_NAME} SHARED ${SRCS})
set_target_properties(${LIBRARY_NAME} PROPERTIES PREFIX "" SUFFIX ${LIB_SUFFIX})
install(TARGETS ${LIBRARY_NAME}
	DESTINATION ${PD_EXTRA_DIR}
	PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
)
set(LIBRARY_HELP_PATCH "${CMAKE_CURRENT_SOURCE_DIR}/${LIBRARY_NAME}-help.pd")
message("Checking help patch ${LIBRARY_HELP_PATCH}")
if(EXISTS ${LIBRARY_HELP_PATCH})
	message("Copying help patch ${LIBRARY_HELP_PATCH}")
	install(FILES ${LIBRARY_HELP_PATCH}
		DESTINATION ${PD_EXTRA_DIR}
		PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
	)
endif()

endmacro(add_pd_library LIBRARY_NAME)
