# - Find PureData
# Find the PureData includes and app
# This module defines
#  PD_INCLUDE_DIR, where to find m_pd.h
#  PD_INCLUDE_DIR, where to find externals
#  PD_FOUND, If false, do not try to use PUREDATA.
# also defined, but not for general use are
#=============================================================================
# Copyright 2017 Christian Frisson
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distributed this file outside of CMake, substitute the full
#  License text for the above reference.)

if(APPLE)
	find_program(PD_PATH NAMES Pd Pd-l2ork)
	#message("PD_PATH ${PD_PATH}")
	if(EXISTS "${PD_PATH}")
	  get_filename_component(PD_APP_STEM "${PD_PATH}" NAME)
	  set(PD_PATH_STEM ${PD_APP_STEM})
	  if("${PD_APP_STEM}" MATCHES "nwjs")
		  set(PD_PATH_STEM "app.nw")
	  endif()
          get_filename_component(PD_PARENT_PATH "${PD_PATH}" PATH)
	  get_filename_component(PD_PARENT_PATH_STEM "${PD_PARENT_PATH}" NAME)
	  #message("PD_PARENT_PATH_STEM ${PD_PARENT_PATH_STEM}")
	  if("${PD_PARENT_PATH_STEM}" MATCHES "MacOS")
		  message("We have a bundle")
		  get_filename_component(PD_BUNDLE_ROOT "${PD_PARENT_PATH}" PATH)
		  #message("PD_BUNDLE_ROOT ${PD_BUNDLE_ROOT}")
		  find_file(PD_HEADER NAMES m_pd.h PATHS "${PD_BUNDLE_ROOT}/Resources/${PD_PATH_STEM}/include")
		  #message("PD_HEADER ${PD_HEADER}")
		  if(EXISTS "${PD_HEADER}")
			  get_filename_component(PD_INCLUDE_DIR "${PD_HEADER}" PATH)
			  #message("PD_INCLUDE_DIR ${PD_INCLUDE_DIR}")
			  set(PD_EXTRA_DIR "${PD_BUNDLE_ROOT}/Resources/${PD_PATH_STEM}/extra" )
		  else()
			  message(FATAL_ERROR "Could not find PureData header m_pd.h")
		  endif()
	  endif()
	else()
		message(FATAL_ERROR "Could not find PureData app")
	endif()
elseif(UNIX)
	find_program(PD_PATH NAMES pd-l2ork pd HINTS "/opt/purr-data/bin")
	message("PD_PATH ${PD_PATH}")
	if(EXISTS "${PD_PATH}")
		get_filename_component(PD_BIN "${PD_PATH}" DIRECTORY)
		get_filename_component(PD_ROOT "${PD_BIN}" DIRECTORY)
		message("PD_ROOT ${PD_ROOT}")

		get_filename_component(PD_NAME "${PD_PATH}" NAME_WE)
		if(NOT PD_NAME)
			message(FATAL_ERROR "Could not guess PureData application name ${PD_NAME} from ${PD_PATH}")
		endif()
		message("PD_NAME ${PD_NAME}")
		find_file(PD_HEADER NAMES m_pd.h HINTS "${PD_ROOT}/include")
	  	if(EXISTS "${PD_HEADER}")
		  get_filename_component(PD_INCLUDE_DIR "${PD_HEADER}" PATH)
		  message("PD_INCLUDE_DIR ${PD_INCLUDE_DIR}")
	  	else()
		  message(FATAL_ERROR "Could not find PureData header m_pd.h")
	  	endif()
		set(PD_EXTRA_DIR "${PD_ROOT}/lib/${PD_NAME}/extra")
	  	if(EXISTS "${PD_EXTRA_DIR}")
		  message("PD_EXTRA_DIR ${PD_EXTRA_DIR}")
	  	else()
		  message(FATAL_ERROR "Could not find PureData extra path")
	  	endif()
	else()
		message(FATAL_ERROR "Could not find PureData binary")
	endif()
else()
        message(FATAL_ERROR "Finding PureData currently not implemented outside macOS and Linux")
endif()

# handle the QUIETLY and REQUIRED arguments and set PD_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PUREDATA DEFAULT_MSG PD_INCLUDE_DIR PD_EXTRA_DIR)

MARK_AS_ADVANCED(PD_INCLUDE_DIR PD_EXTRA_DIR)
