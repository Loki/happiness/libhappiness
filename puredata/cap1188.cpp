/**
 * @file cap1188.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from cap1188
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "m_pd.h"

#include "cap1188.h"
#include "gpio_irq.h"

static t_class *cap1188_class;

typedef struct _cap1188 {
    t_object  x_obj;
    t_outlet* x_outlet;
    I2cmple* x_i2c;
    Cap1188 x_slider;
    Gpio_irq x_irq;
    int x_childthread_running;
    pthread_t x_childthread;
} t_cap1188;

static void *cap1188_main(void* zz)
{
    t_cap1188 *x = (t_cap1188 *)zz;
    while (x->x_childthread_running == 1) {
		x->x_irq.waitForIRQ();
		int output_list_size = 8;
		int8_t buff [output_list_size];
        int r = x->x_slider.read_delta_count(buff);
        if (r >= 0) {
            t_atom output_list[output_list_size];
            for (int i=0; i< output_list_size;i++) {
                SETFLOAT(output_list+i,buff[i]);
            }
            outlet_list(x->x_outlet, &s_, output_list_size, output_list);
        }
        else{
            printf("Error during read position\n");
        }
        x->x_slider.clear_interrupt_bit();
    }
    return 0;
}

void *cap1188_new(void)
{
    t_cap1188 *x = (t_cap1188 *)pd_new(cap1188_class);

    x->x_outlet = outlet_new(&x->x_obj, &s_list);

    x->x_i2c = new I2cmple();
    x->x_irq = Gpio_irq(Gpio_irq::GPIO_23, Gpio_irq::LOW, Gpio_irq::RISING);
    x->x_slider = Cap1188(x->x_i2c, Cap1188::CAP1188_ADDR_DEFAULT);
    int r = x->x_slider.enable_led_debug();
    if (r < 0) {
        printf("Error during enable led debug\n");
        return (void*)0;
    }
    r = x->x_slider.set_sensor_sensitivity(0b00000100);
    if (r < 0) {
        printf("Error during set sensitivity\n");
        return (void*)0;
    }

    x->x_slider.clear_interrupt_bit();

    x->x_childthread_running = 1;
    pthread_create(&x->x_childthread, 0, cap1188_main, x);

    return (void *)x;
}

static void cap1188_free(t_cap1188 *x)
{
    x->x_childthread_running = 0;
    void *threadrtn;
    if (pthread_join(x->x_childthread, &threadrtn))
        error("cap1188_free: child thread join failed");
}

void cap1188_sensitivity(t_cap1188 *x, t_floatarg f)
{
  printf("sensitivity %f\n",f);
  int r = x->x_slider.set_sensor_sensitivity(f);
  if(r<0){
	  printf("Error setting sensitivity, aborting\n");
	  return;
  }
}

void cap1188_threshold(t_cap1188 *x, t_floatarg f)
{
  printf("threshold %f\n",f);
  int8_t thres[8];
  for(int t=0;t<8;t++){
	  thres[t]=f;
  }
  int r = x->x_slider.set_sensor_threshold(thres);
  if(r<0){
	  printf("Error setting threshold, aborting\n");
	  return;
  }
}

void cap1188_reset(t_cap1188 *x) {
	printf("calibration \n");
	int r = x->x_slider.recalibrate_sensors();
	if(r<0) {
		printf("Error calibrating, aborting\n");
		return;
	}
	printf("Calibrated\n");
}

void cap1188_setup(void)
{
    cap1188_class = class_new(gensym("cap1188"),
                              (t_newmethod)cap1188_new,
                              (t_method)cap1188_free, sizeof(t_cap1188),
                              CLASS_DEFAULT, (t_atomtype)0);
    class_addmethod(cap1188_class, (t_method)cap1188_reset, gensym("reset"), (t_atomtype) 0, 0);
    class_addmethod(cap1188_class,
        (t_method)cap1188_sensitivity, gensym("sensitivity"),
        A_DEFFLOAT, 0);
    class_addmethod(cap1188_class,
        (t_method)cap1188_threshold, gensym("threshold"),
        A_DEFFLOAT, 0);
    //TODO test this
    class_sethelpsymbol(cap1188_class, gensym("cap1188-help"));
}

#ifdef __cplusplus
}
#endif
