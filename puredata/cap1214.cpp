/**
 * @file cap1214.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from cap1214
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "m_pd.h"

#include "cap1214.h"
#include "gpio_irq.h"

static t_class *cap1214_class;

typedef struct _cap1214 {
    t_object  x_obj;
    t_outlet *cursor_outlet,*electrodes_outlet;
    I2cmple* x_i2c;
    Cap1214 x_slider;
    Gpio_irq x_irq;
    int x_resolution;
} t_cap1214;

static void *cap1214_main(void* zz) {
    t_cap1214 *x = (t_cap1214 *)zz;
    {
        int output_list_size = 2;
        float buff [output_list_size];
        int r = x->x_slider.read_cursor_position(buff, x->x_resolution);
        if (r >= 0) {
            t_atom output_list[output_list_size];
            for (int i=0; i< output_list_size;i++) {
                SETFLOAT(output_list+i,buff[i]);
            }
            outlet_list(x->cursor_outlet, &s_, output_list_size, output_list);
        }
        else{
            printf("Error during read position\n");
        }
    }
    {
        int output_list_size = 11;
        int8_t buff [output_list_size];
        int r = x->x_slider.read_delta_count(buff);
        if (r >= 0) {
            t_atom output_list[output_list_size];
            for (int i=0; i< output_list_size;i++) {
                SETFLOAT(output_list+i,buff[i]);
            }
            outlet_list(x->electrodes_outlet, &s_, output_list_size, output_list);
        }
        else{
            printf("Error while reading position\n");
        }
    }
    return 0;
}

void *cap1214_new(void) {
    t_cap1214 *x = (t_cap1214 *)pd_new(cap1214_class);

    x->cursor_outlet = outlet_new(&x->x_obj, &s_list);
    x->electrodes_outlet = outlet_new(&x->x_obj, &s_list);

    x->x_i2c = new I2cmple();
    x->x_slider = Cap1214(x->x_i2c);
    x->x_resolution = 100;
    int r = x->x_slider.set_sensor_sensitivity(0b00000100);
    /*if (r < 0) {
        printf("Error during set sensitivity\n");
        return (void*)0;
    }*/

    return (void *)x;
}

static void cap1214_free(t_cap1214 *x) {}

void cap1214_sensitivity(t_cap1214 *x, t_floatarg f) {
    printf("sensitivity %f\n",f);
    int r = x->x_slider.set_sensor_sensitivity(f);
    if(r<0) {
        printf("Error setting sensitivity, aborting\n");
        return;
    }
}

void cap1214_threshold(t_cap1214 *x, t_floatarg f) {
    printf("threshold %f\n",f);
    int8_t thres[8];
    for(int t=0;t<8;t++) {
        thres[t]=f;
    }
    int r = x->x_slider.set_sensor_threshold(thres);
    if(r<0) {
        printf("Error setting threshold, aborting\n");
        return;
    }
}

void cap1214_resolution(t_cap1214 *x, t_floatarg f) {
    printf("resolution %d\n",(int)f);
    if(f<=0) {
        printf("Error setting resolution, aborting\n");
        return;
    }
    x->x_resolution = (int)f;
}

void cap1214_reset(t_cap1214 *x) {
    printf("calibration \n");
    int r = x->x_slider.recalibrate_sensors();
    if(r<0) {
        printf("Error calibrating, aborting\n");
        return;
    }
    printf("Calibrated\n");
}

void cap1214_setup(void) {
    cap1214_class = class_new(gensym("cap1214"),
                              (t_newmethod)cap1214_new,
                              (t_method)cap1214_free, sizeof(t_cap1214),
                              CLASS_DEFAULT, (t_atomtype)0);
    class_addbang(cap1214_class, cap1214_main);
    class_addmethod(cap1214_class, (t_method)cap1214_reset, gensym("reset"), (t_atomtype) 0, 0);
    class_addmethod(cap1214_class,
                    (t_method)cap1214_sensitivity, gensym("sensitivity"),
                    A_DEFFLOAT, 0);
    class_addmethod(cap1214_class, (t_method)cap1214_threshold, gensym("threshold"), A_DEFFLOAT, 0);
    class_addmethod(cap1214_class, (t_method)cap1214_resolution, gensym("resolution"), A_DEFFLOAT, 0);
}

#ifdef __cplusplus
}
#endif
