/**
 * @file cap1188.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from cap1188
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "m_pd.h"


#include "button_set.h"
#include "cap1188.h"
#include "gpio_irq.h"

static t_class *happbutton_class;

typedef struct _happbutton {
	t_object  x_obj;
    t_outlet* x_outlet;
    I2cmple* x_i2c;
    Cap1188* x_slider;
    Gpio_irq* x_irq;
    ButtonSet x_bs;
} t_happbutton;


static void *happbutton_main(void* zz) {
	t_happbutton *x = (t_happbutton *)zz;
	int output_list_size = 8;
	int8_t buff [output_list_size];
	int r = x->x_bs.read_delta_count(buff);
	if (r >= 0) {
		t_atom output_list[output_list_size];

		for (int i=0; i< output_list_size;i++) {
			SETFLOAT(output_list+i,buff[i]);
		}
		outlet_list(x->x_outlet, &s_, output_list_size, output_list);
	}
	else{
		printf("Error during read position\n");
	}
    return 0;
}

void *happbutton_new(void) {
    t_happbutton *x = (t_happbutton *)pd_new(happbutton_class);
    x->x_outlet = outlet_new(&x->x_obj, &s_list);
    x->x_i2c = new I2cmple();
    x->x_irq = new Gpio_irq(Gpio_irq::GPIO_23, Gpio_irq::LOW, Gpio_irq::RISING);
    x->x_slider = new Cap1188(x->x_i2c, Cap1188::CAP1188_ADDR_DEFAULT);
    x->x_bs = ButtonSet(x->x_slider, x->x_irq, ((uint8_t) 6));
    int r = x->x_slider->set_sensor_sensitivity(0b00000100);
    if (r < 0) {
        printf("Error during set sensitivity\n");
        return (void*)0;
    }

    return (void *)x;
}

static void happbutton_free(t_happbutton *x) {}

void happbutton_sensitivity(t_happbutton *x, t_floatarg f) {
  printf("sensitivity %f\n",f);
  int r = x->x_slider->set_sensor_sensitivity(f);
  if(r<0){
	  printf("Error setting sensitivity, aborting\n");
	  return;
  }
}

void happbutton_threshold(t_happbutton *x, t_floatarg f) {
	printf("threshold %f\n",f);
	int8_t thres[8];
	for(int t=0;t<8;t++){
		thres[t]=f;
	}
	int r = x->x_slider->set_sensor_threshold(thres);
	if(r<0){
		printf("Error setting threshold, aborting\n");
		return;
	}
}

void happbutton_polling_threshold(t_happbutton *x, t_floatarg f) {
	printf("polling threshold %f\n", f);
	int8_t thres[8];
	for(int t=0;t<8;t++){
		thres[t]=f;
	}
	int r = x->x_bs.set_polling_threshold(thres);
	if(r<0){
		printf("Error setting polling threshold, aborting\n");
		return;
	}
}

void happbutton_setup(void) {
	happbutton_class = class_new(gensym("happbutton"),
                              (t_newmethod)happbutton_new,
                              (t_method)happbutton_free, sizeof(t_happbutton),
                              CLASS_DEFAULT, (t_atomtype)0);

    class_addbang(happbutton_class, happbutton_main);
    class_addmethod(happbutton_class,
        (t_method)happbutton_sensitivity, gensym("sensitivity"),
        A_DEFFLOAT, 0);
    class_addmethod(happbutton_class,
        (t_method)happbutton_threshold, gensym("threshold"),
        A_DEFFLOAT, 0);
    class_addmethod(happbutton_class,
		(t_method)happbutton_polling_threshold, gensym("pollthreshold"),
		A_DEFFLOAT, 0);
}

#ifdef __cplusplus
}
#endif
