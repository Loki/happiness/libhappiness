/**
 * @file drv2667.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from drv2667
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include <math.h>
#include "m_pd.h"

#include "drv2667.h"
#include "tca9548a.h"

static t_class *drv2667_class;

typedef struct _drv2667 {
    t_object	x_obj;
    t_outlet*	x_outlet;
    I2cmple*	x_i2c;
    Drv2667		x_drv;
    Tca9548a	x_tca;
    uint8_t		x_tcaaddr;
} t_drv2667;

void *drv2667_new(t_symbol *s, int argc, t_atom *argv) {
    t_drv2667 *x = (t_drv2667 *)pd_new(drv2667_class);

	if (!(argc > 1 && argv[0].a_type == A_FLOAT && argv[1].a_type == A_FLOAT)) {
		printf("Usage: drv2667 tcaAddr mode [mode_arg1, mode_arg2...]\n");
		//TODO add other mode
		printf("Mode:\n 1 - analog\n 2 - DirectPlayback\n Other Not supported");
		return (void*)0;
	}

	// Allow object creation for opening patches when not on raspberry pi (arm)
	#ifndef __arm__
	return (void *)x;
	#endif

	x->x_i2c = new I2cmple();
	x->x_tca = Tca9548a(x->x_i2c);

    //Set TCA addr
    if (argv[0].a_w.w_float >= 0 && argv[0].a_w.w_float <= 8) {
		x->x_tcaaddr = 0b00000001 << (int)(argv[0].a_w.w_float);
	} else {
		printf("Error while setting tcaAddr");
		//TODO handle error
	}

	x->x_tca.select(x->x_tcaaddr);
    x->x_drv = Drv2667(x->x_i2c, Drv2667::DRV2667_100V_40_7DB);

	int r;
	switch((int) argv[1].a_w.w_float) {
		case 1:
			printf("Initializing in analog mode\n");
			r = x->x_drv.init_analog();
			break;
		case 2:
			printf("Initializing in digital mode\n");
			//TODO add parameter for this
			r = x->x_drv.init_digital(Drv2667::DRV2667_TO_20MS);
			break;
	}

    if (r < 0) {
        printf("Error trying to init drv2667 \n");
        return (void*)0;
    }
	//DEBUG
	printf("End of function\n");
	//DEBUG
	
    return (void *)x;
}

static void drv2667_free(t_drv2667 *x) {
	//TODO add deconfiguration of drv2667 (turn off analog mode)
}

void drv2667_gain(t_drv2667 *x, t_floatarg f) {
	printf("gain %f\n",f);
	int r = x->x_drv.set_gain(static_cast<Drv2667::drv2667_gain_t>(f));
	if(r<0) {
		printf("Error setting gain, aborting\n");
		return;
	}
}

void drv2667_setup(void) {
    drv2667_class = class_new(gensym("drv2667"),
                              (t_newmethod)drv2667_new,
                              (t_method)drv2667_free, sizeof(t_drv2667),
                              CLASS_DEFAULT, A_GIMME, 0);
	class_addmethod(drv2667_class,
					(t_method)drv2667_gain, gensym("gain"),
					A_DEFFLOAT, 0);
}

#ifdef __cplusplus
}
#endif
