/*(utf8)
1€ filter for pure data
Jonathan Aceituno <join@oin.name>
*/

#if __GNUC__ >= 4
#define EXPORT __attribute__((visibility("default")))
#else
#define EXPORT
#endif

#include <cmath>

template <typename T = double>
struct low_pass_filter {
	low_pass_filter() : hatxprev(0), xprev(0), hadprev(false) {}
	T operator()(T x, T alpha) {
		T hatx;
		if(hadprev) {
			hatx = alpha * x + (1-alpha) * hatxprev;
		} else {
			hatx = x;
			hadprev = true;
		}
		hatxprev = hatx;
		xprev = x;
		return hatx;
	}
	T hatxprev;
	T xprev;
	bool hadprev;
};

template <typename T = double, typename timestamp_t = double>
struct one_euro_filter {
	one_euro_filter(double _freq, T _mincutoff, T _beta, T _dcutoff) : freq(_freq), mincutoff(_mincutoff), beta(_beta), dcutoff(_dcutoff), last_time_(-1) {}
	T operator()(T x, timestamp_t t = -1) {
		T dx = 0;

		if(last_time_ != -1 && t != -1 && t != last_time_) {
			freq = 1.0 / (t - last_time_);
		}
		last_time_ = t;

		if(xfilt_.hadprev)
			dx = (x - xfilt_.xprev) * freq;

		T edx = dxfilt_(dx, alpha(dcutoff));
		T cutoff = mincutoff + beta * std::abs(static_cast<double>(edx));
		return xfilt_(x, alpha(cutoff));
	}

	double freq;
	T mincutoff, beta, dcutoff;
private:
	T alpha(T cutoff) {
		T tau = 1.0 / (2 * M_PI * cutoff);
		T te = 1.0 / freq;
		return 1.0 / (1.0 + tau / te);
	}

	timestamp_t last_time_;
	low_pass_filter<T> xfilt_, dxfilt_;
};

#ifdef __cplusplus
extern "C" {
#endif
#include "m_pd.h"

static t_class *oneeurofilter_class;

typedef struct _1efilter {
	t_object x_obj;
	one_euro_filter<float> *filter;
	t_outlet *f_out;
} t_1efilter;

void *oneeurofilter_new(t_symbol*, int argc, t_atom *argv) {
	// arguments: freq mincutoff beta dcutoff
	t_1efilter *x = (t_1efilter *)pd_new(oneeurofilter_class);
	float freq = 120;
	float mincutoff = 1;
	float beta = 1;
	float dcutoff = 1;
	switch(argc) {
		default:
		case 4:
			dcutoff = atom_getfloat(argv + 3);
		case 3:
			beta = atom_getfloat(argv + 2);
		case 2:
			mincutoff = atom_getfloat(argv + 1);
		case 1:
			freq = atom_getfloat(argv);
		case 0:
			;
	}

	x->filter = new one_euro_filter<float>(freq, mincutoff, beta, dcutoff);

	inlet_new(&x->x_obj, &x->x_obj.ob_pd, 0, 0);
	x->f_out = outlet_new(&x->x_obj, &s_float);

	return (void *)x;
}

void oneeurofilter_float(t_1efilter *x, t_floatarg v) {
	outlet_float(x->f_out, x->filter->operator()(v));
}

void oneeurofilter_filter(t_1efilter *x, t_floatarg v, t_floatarg t) {
	outlet_float(x->f_out, x->filter->operator()(v, t));
}

void oneeurofilter_mincutoff(t_1efilter *x, t_floatarg f) {
	x->filter->mincutoff = f;
}

void oneeurofilter_beta(t_1efilter *x, t_floatarg f) {
	x->filter->beta = f;
}

void oneeurofilter_dcutoff(t_1efilter *x, t_floatarg f) {
	x->filter->dcutoff = f;
}

void oneeurofilter_freq(t_1efilter *x, t_floatarg f) {
	x->filter->freq = f;
}

void oneeurofilter_setup(void) {
	oneeurofilter_class = class_new(gensym("oneeurofilter"), (t_newmethod)oneeurofilter_new, 0, sizeof(t_1efilter), CLASS_DEFAULT, A_GIMME, 0);
	class_addfloat(oneeurofilter_class, oneeurofilter_float);
	class_addmethod(oneeurofilter_class, (t_method)oneeurofilter_filter, gensym("filter"), A_DEFFLOAT, A_DEFFLOAT, 0);
	class_addmethod(oneeurofilter_class, (t_method)oneeurofilter_freq, gensym("freq"), A_DEFFLOAT, 0);
	class_addmethod(oneeurofilter_class, (t_method)oneeurofilter_mincutoff, gensym("mincutoff"), A_DEFFLOAT, 0);
	class_addmethod(oneeurofilter_class, (t_method)oneeurofilter_beta, gensym("beta"), A_DEFFLOAT, 0);
	class_addmethod(oneeurofilter_class, (t_method)oneeurofilter_dcutoff, gensym("dcutoff"), A_DEFFLOAT, 0);

	class_sethelpsymbol(oneeurofilter_class, gensym("oneeurofilter"));
}

#ifdef __cplusplus
}
#endif

