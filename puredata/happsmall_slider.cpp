
/**
 * @file happsmall_slider.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from cap1188
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "m_pd.h"

#include "cap1188.h"
#include "gpio_irq.h"

static t_class *happsmall_slider_class;

typedef struct _happsmall_slider {
	t_object  x_obj;
    t_outlet* x_outlet;
    I2cmple* x_i2c;
    Cap1188* x_slider;
    Gpio_irq* x_irq;
} t_happsmall_slider;


static void *happsmall_slider_main(void* zz) {
	t_happsmall_slider *x = (t_happsmall_slider *)zz;
	int output_list_size = 11;
	int8_t buff [output_list_size];
	int r = x->x_slider->read_delta_count(buff);
	if (r >= 0) {
		t_atom output_list[output_list_size];

		for (int i=0; i< output_list_size;i++) {
			SETFLOAT(output_list+i,buff[i]);
		}
		outlet_list(x->x_outlet, &s_, output_list_size, output_list);
	}
	else{
		printf("Error while reading position\n");
	}
    return 0;
}

void *happsmall_slider_new(t_symbol *s, int argc, t_atom *argv) {
    t_happsmall_slider *x = (t_happsmall_slider *)pd_new(happsmall_slider_class);
    x->x_outlet = outlet_new(&x->x_obj, &s_list);
    x->x_i2c = new I2cmple();
    Cap1188::cap1188_addr_t addr = Cap1188::CAP1188_ADDR_DEFAULT;
    if (argc > 0) {
		switch ((int)(argv[0].a_w.w_float)) {
			case 1:
				addr = Cap1188::CAP1188_ADDR_3V;
				break;
			case 2:
				addr = Cap1188::CAP1188_ADDR_DEFAULT;
				break;
			case 3:
				addr = Cap1188::CAP1188_ADDR_600K;
				break;
			case 4:
				addr = Cap1188::CAP1188_ADDR_300K;
				break;
			case 5:
				addr = Cap1188::CAP1188_ADDR_180K;
				break;
			default:
				printf("Error: Unsupported parameter\n");
				break;
		}
		printf("Using selected addr: %d\n", addr);
	} else {
		printf("Using default addr\n");
	}
	x->x_slider = new Cap1188(x->x_i2c, addr);

    //TODO check this value for sensitivity
    int r = x->x_slider->set_sensor_sensitivity(0b00000100);
    if (r < 0) {
        printf("Error while setting sensitivity\n");
        return (void*)0;
    }

    return (void *)x;
}

static void happsmall_slider_free(t_happsmall_slider *x) {}

void happsmall_slider_sensitivity(t_happsmall_slider *x, t_floatarg f) {
  printf("sensitivity %f\n",f);
  int r = x->x_slider->set_sensor_sensitivity(f);
  if(r<0){
	  printf("Error setting sensitivity, aborting\n");
	  return;
  }
}

void happsmall_slider_threshold(t_happsmall_slider *x, t_floatarg f) {
	printf("threshold %f\n",f);
	int8_t thres[8];
	for(int t=0;t<8;t++){
		thres[t]=f;
	}
	int r = x->x_slider->set_sensor_threshold(thres);
	if(r<0){
		printf("Error setting threshold, aborting\n");
		return;
	}
}

void happsmall_slider_reset(t_happsmall_slider *x) {
    printf("calibration \n");
    int r = x->x_slider->recalibrate_sensors();
    if(r<0) {
        printf("Error calibrating, aborting\n");
        return;
    }
    printf("Calibrated\n");
}

void happsmall_slider_setup(void) {
	happsmall_slider_class = class_new(gensym("happsmall_slider"),
                              (t_newmethod)happsmall_slider_new,
                              (t_method)happsmall_slider_free, sizeof(t_happsmall_slider),
                              CLASS_DEFAULT, A_GIMME, (t_atomtype)0);

    class_addbang(happsmall_slider_class, happsmall_slider_main);
    class_addmethod(happsmall_slider_class, (t_method)happsmall_slider_reset, gensym("reset"), (t_atomtype) 0, 0);
    class_addmethod(happsmall_slider_class,
        (t_method)happsmall_slider_sensitivity, gensym("sensitivity"),
        A_DEFFLOAT, 0);
    class_addmethod(happsmall_slider_class,
        (t_method)happsmall_slider_threshold, gensym("threshold"),
        A_DEFFLOAT, 0);
}

#ifdef __cplusplus
}
#endif
