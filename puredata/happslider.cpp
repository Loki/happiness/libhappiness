/**
 * @file happslider.cpp
 * @authors Christian Frisson, Julien Decaudin
 * @brief PureData object to receive input from cap1214
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>
#include "m_pd.h"

#include "cap1214.h"
#include "gpio_irq.h"

static t_class *happslider_class;

typedef struct _happslider {
	t_object  x_obj;
    t_outlet* x_outlet;
    I2cmple* x_i2c;
    Cap1214* x_slider;
    Gpio_irq* x_irq;
} t_happslider;


static void *happslider_main(void* zz) {
	t_happslider *x = (t_happslider *)zz;
	int output_list_size = 11;
	int8_t buff [output_list_size];
	int r = x->x_slider->read_delta_count(buff);
	if (r >= 0) {
		t_atom output_list[output_list_size];

		for (int i=0; i< output_list_size;i++) {
			SETFLOAT(output_list+i,buff[i]);
		}
		outlet_list(x->x_outlet, &s_, output_list_size, output_list);
	}
	else{
		printf("Error while reading position\n");
	}
    return 0;
}

void *happslider_new(void) {
    t_happslider *x = (t_happslider *)pd_new(happslider_class);
    x->x_outlet = outlet_new(&x->x_obj, &s_list);
    x->x_i2c = new I2cmple();
    x->x_slider = new Cap1214(x->x_i2c);
    //TODO check this value for sensitivity
    int r = x->x_slider->set_sensor_sensitivity(0b00000100);
    if (r < 0) {
        printf("Error while setting sensitivity\n");
        return (void*)0;
    }

    return (void *)x;
}

static void happslider_free(t_happslider *x) {}

void happslider_sensitivity(t_happslider *x, t_floatarg f) {
  printf("sensitivity %f\n",f);
  int r = x->x_slider->set_sensor_sensitivity(f);
  if(r<0){
	  printf("Error setting sensitivity, aborting\n");
	  return;
  }
}

void happslider_threshold(t_happslider *x, t_floatarg f) {
	printf("threshold %f\n",f);
	int8_t thres[8];
	for(int t=0;t<8;t++){
		thres[t]=f;
	}
	int r = x->x_slider->set_sensor_threshold(thres);
	if(r<0){
		printf("Error setting threshold, aborting\n");
		return;
	}
}

void happslider_reset(t_happslider *x) {
    printf("calibration \n");
    int r = x->x_slider->recalibrate_sensors();
    if(r<0) {
        printf("Error calibrating, aborting\n");
        return;
    }
    printf("Calibrated\n");
}

void happslider_setup(void) {
	happslider_class = class_new(gensym("happslider"),
                              (t_newmethod)happslider_new,
                              (t_method)happslider_free, sizeof(t_happslider),
                              CLASS_DEFAULT, (t_atomtype)0);

    class_addbang(happslider_class, happslider_main);
    class_addmethod(happslider_class, (t_method)happslider_reset, gensym("reset"), (t_atomtype) 0, 0);
    class_addmethod(happslider_class,
        (t_method)happslider_sensitivity, gensym("sensitivity"),
        A_DEFFLOAT, 0);
    class_addmethod(happslider_class,
        (t_method)happslider_threshold, gensym("threshold"),
        A_DEFFLOAT, 0);
}

#ifdef __cplusplus
}
#endif
