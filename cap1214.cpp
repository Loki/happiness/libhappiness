/**
 * @file cap1214.cpp
 * @authors Julien Decaudin
 * @brief CAP1214 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "cap1214.h"

Cap1214::Cap1214(I2C* i2cH) : i2c(i2cH) {
	//Init cap struct
	this->addr = CAP1214_ADDR_DEFAULT;
	//TODO move this to slider class once its created
	//Disable grouped sensor
    i2c->write8(addr, CAP1214_GLOBAL_CONFIG_2, 0x01 << 1, 0b00000010);
	//Enable repeat rate
    i2c->write8(addr, CAP1214_GLOBAL_CONFIG, 0x01 << 2, 0b00000100);
}

int Cap1214::clear_interrupt_bit() {
    return i2c->write8(addr, CAP1214_MAIN_CTRL, 0x00, 0b00000001);
}

int Cap1214::recalibrate_sensors() {
	int r;
	r = i2c->write8(this->addr, CAP1214_FRCE_CALIB, 0xFF, 0xFF);
	if (r < 0) { return r; }
	uint8_t res = 0xFF;
	while ((res = i2c->read8(this->addr, CAP1214_FRCE_CALIB, &res)) != 0x00) {}
	return r;
}

int Cap1214::set_sensor_sensitivity(uint8_t sensitivity) {
    return i2c->write8(addr, CAP1214_DATA_SENSITIV, sensitivity << 4, 0b01110000);
}

int Cap1214::set_sensor_threshold(int8_t *list) {
	return i2c->write_bytes(this->addr, CAP1214_SENSOR_THRES, (uint8_t*)list, 8);
}

int Cap1214::read_delta_count(int8_t *buffer) {
	int r;
	uint8_t buff[14];
    r = i2c->read_bytes(addr, CAP1214_DELTA_COUNT, buff, 14);
	if (r < 0) { return r; }
	for (int i=0; i < 11; i++) {
		buffer[i] = (buff[i+3] ^ 0x80) - 0x80;
	}
	return r;
}

int Cap1214::read_cursor_position(float *buffer, int resolution) {
	int r;
	int nbElectrode = 11;
	int8_t S_befor, S_after, S;
	int8_t buff[11];
	r = read_delta_count(buff);
	if (r < 0) { return r; }

	int8_t pos, max;
	max = -1;
	pos = -1;
	for (int i=0; i < nbElectrode;i++) {
		//Clear negative value
		if (buff[i] < 0) {
			buff[i] = 0;
		}
		if (max < buff[i]) {
			pos = i;
			max = buff[i];
		}
	}

	if (max == 0) {
		*buffer = 0.0;
		return r;
	} else if (pos==0) {//First electrode
		S_befor = 0;
		S_after = buff[1];
	} else if (pos == (nbElectrode - 1)) {//Last electrode
		S_befor = buff[pos-1];
		S_after = 0;
	} else {
		S_befor = buff[pos-1];
		S_after = buff[pos+1];
	}

	S = buff[pos];
    buffer[0] = ((float)((float)(S_after-S_befor)/(S+S_after+S_befor)+pos) * (float)resolution/(nbElectrode - 1)); // position at max interpolated value
    buffer[1] = ((float)((float)(S_after-S_befor)/(S+S_after+S_befor))+(float)max); // max interpolated value
    return r;
}
