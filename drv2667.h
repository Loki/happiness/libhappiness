/**
 * @file drv2667.h
 * @authors Julien Decaudin
 * @brief DRV2667 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once

#include "i2cmple.h"

#define DRV2667_ADDR	0x59

class Drv2667 {
	public:
		/**
		 * DRV2667 control registers.
		 * List of addresses of control registers for the DRV2667.
		 */
		enum drv2667_register_t {
			INFO					= 0x00,/**< Register address for general info. (XXXXXabc) Illegal address bit (a), Fifo empty (b), Fifo full (c).*/
			CONFIG_1				= 0x01,/**< Register address for configuration (1/2). (Xaaaabcc) Device identifier (aaaa), Input source(b), Gain (cc).*/
			CONFIG_2				= 0x02,/**< Register address for configuration (2/2). (abXXccde) Reset (a), Stand by (b), Timeout (cc), Override ??? (d), Start playback (e).*/
			WAVEFORM_0				= 0x03,/**< Register address for 1st effect to be played*/
			WAVEFORM_1				= 0x04,/**< Register address for 2nd effect to be played*/
			WAVEFORM_2				= 0x05,/**< Register address for 3rd effect to be played*/
			WAVEFORM_3				= 0x06,/**< Register address for 4th effect to be played*/
			WAVEFORM_4				= 0x07,/**< Register address for 5th effect to be played*/
			WAVEFORM_5				= 0x08,/**< Register address for 6th effect to be played*/
			WAVEFORM_6				= 0x09,/**< Register address for 7th effect to be played*/
			WAVEFORM_7				= 0x0A,/**< Register address for 8th effect to be played*/
			FIFO					= 0x0B,/**< Register address for FIFO. Write this register to add data to the FIFO.*/
			PAGE_REG				= 0xFF /**< Page register address. Write to this register to change the memory page of the DRV2667*/
		};

		/**
		 * DRV2667 amplifier gain.
		 * List of supported gain for analog (in dB) or digital playback (Vpp).
		 */
		enum drv2667_gain_t {
			DRV2667_25V_28_8DB		= 0,
			DRV2667_50V_34_8DB		= 1,
			DRV2667_75V_38_4DB		= 2,
			DRV2667_100V_40_7DB		= 3
		};

		/**
		 * DRV2667 timeout.
		 * List of supported timeout for FIFO.
		 */
		enum drv2667_timeout_t {
			DRV2667_TO_5MS			= 0,
			DRV2667_TO_10MS			= 1,
			DRV2667_TO_15MS			= 2,
			DRV2667_TO_20MS			= 3
		};

		enum drv2667_synthesis_envelope_t {
			DRV2667_NO_ENV	= 0,
			DRV2667_32MS	= 1,
			DRV2667_64MS	= 2,
			DRV2667_96MS	= 3,
			DRV2667_128MS	= 4,
			DRV2667_160MS	= 5,
			DRV2667_192MS	= 6,
			DRV2667_224MS	= 7,
			DRV2667_256MS	= 8,
			DRV2667_512MS	= 9,
			DRV2667_768MS	= 10,
			DRV2667_1024MS	= 11,
			DRV2667_1280MS	= 12,
			DRV2667_1536MS	= 13,
			DRV2667_1792MS	= 14,
			DRV2667_2048MS	= 15
		};

		/**
		 * Structure for Waveform synthesis data.
		 * Represent the waveform synthesis data as stored in RAM.
		 */
		struct drv2667_waveform_synthesis_t {
			uint8_t amplitude; /**< Amplitude of waveform */
			uint8_t frequency; /**< Frequency of waveform. Sinusoid frequency Hz = 7.8125 * frenquency.*/
			uint8_t duration;  /**<	Duration of waveform. Number of cycles.*/
			uint8_t envelope;  /**< Envelope of waveform. (aaaabbbbb) Ramp-up time (aaaa), Ramp-down time (bbbb)*/
		};

		/**
		 * Structure for DRV2667 RAM effect.
		 * Represent an effect as stored in RAM. An effect is 1 or more waveform played a certain number of time.
		 */
		struct drv2667_effect_t {
			uint8_t start_addr_upper; /**< Start address of effect. (aXXXXbbb) Mode (a), Page Number(bbb)*/
			uint8_t start_addr_lower; /**< Address of effect within page */
			uint8_t stop_addr_upper;  /**< Page number. (XXXXXaaa) Page Number (aaa) */
			uint8_t stop_addr_lower;  /**< Address within page*/
			uint8_t repeat_count;	  /**< Number of time waveform is reapeated. 0 = infinite loop.*/
		};

		/**
		 * Structure for DRV2667 RAM header.
		 * Represent a header as stored in RAM. The header contains a list of effect and it's own size.
		 */
		struct drv2667_header_t {
			drv2667_effect_t *effects;	/**< Ptr to array of effect.*/
			uint16_t neffect;			/**< Number fo effect in header.*/
		};

        Drv2667(I2C* i2cH, drv2667_gain_t gain);

		/**
		 * Set the drv2667 gain
		 */
		 int set_gain(drv2667_gain_t gain);

		/**
		 * Init the drv2667 to use the analog mode.
		 */
		int init_analog();

		/**
		 * Init the drv2667 to use the digital mode.
		 * @param timeout Timeout period for the digital mode.
		 */
		int init_digital(drv2667_timeout_t timeout);

		/**
		 * Switch to given page.
		 * @param pnum The page number.
		 * @notes 0x00 is the control page, so 0x01 is actually RAM page0
		 */
		int switch_page(uint8_t pnum);

		/**
		 * Play the waveform(s) currently stored in ram.
		 * Start playing the effects stored in RAM according to the order specified by the playlist.
		 * @see drv2667_set_playlist()
		 */
		int play();

		/**
		 * Stop playing the waveform.
		 */
		int stop();

		/**
		 * Set the playlist order of all 8 effects.
		 * Specify the order in which the effect should be played by the DRV2667.
		 * @param drv Ptr to the drv struct.
		 * @param list Ptr to a list of 8 number representing effect id.
		 * @see drv2667_play()
		 */
		int set_playlist(uint8_t *list);

		/**
		 * Write all the needed ram data to start playback.
		 * Will write header on page 1 of DRV2667's RAM, then write all waveform on page 2
		 * @param drv Ptr to the drv struct.
		 * @param header Ptr to the header struct.
		 * @param wavef Ptr to an array of waveforms.
		 * @param nbwf Number of waveforms in array.
		 */
		int write_ram_data(drv2667_header_t header,
									drv2667_waveform_synthesis_t *wavef, uint8_t nbwf);
	private:
        I2C* i2c;	/**< I2C communication*/
		uint8_t mode;	/**< Current mode on which DRV2667 is running. 0 is digital, 1 is analog.*/
};
