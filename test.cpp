/**
 * @file test.cpp
 * @authors Julien Decaudin
 * @brief
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/
#include <unistd.h>

#include "tca9548a.h"
#include "drv2667.h"
#include "cap1188.h"
#include "cap1214.h"
#include "mtch6303.h"
#include "gpio_irq.h"
#include "button_set.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#ifdef linux
#include <linux/input.h>
#endif

using namespace std;

static void print_bits(uint8_t x) {
    int i;
    for(i=7; i>=0; i--) {
        (x & (1 << i)) ? putchar('1') : putchar('0');
    }
    printf("\n");
}

void testInterrupt2() {
    I2cmple* i2c = new I2cmple();
	Cap1214 cap = Cap1214(i2c);
	cap.set_sensor_sensitivity(0b00000100);
	int8_t buff[8];
	memset(buff, 0x05, 8);
    cap.set_sensor_threshold(buff);

	Gpio_irq irq = Gpio_irq(Gpio_irq::GPIO_23, Gpio_irq::HIGH, Gpio_irq::RISING);
	//TODO check for error
	for (int i=0;i < 10;i++) {
		irq.waitForIRQ();
		printf("It worked !!!\n");
		cap.clear_interrupt_bit();
	}
	irq.unexport();
}

int cook_test_pinball(Drv2667 drv) {
	int r;
	//Prepare playlist
	uint8_t playlist[8];
	memset(playlist, 0x00, 8);
	playlist[0] = 0x01;
	r = drv.set_playlist(playlist);
	if (r < 0) { return r; }
	//Prepare header
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	header.effects[0].start_addr_upper = 0x81;
	header.effects[0].start_addr_lower = 0x00;
	header.effects[0].stop_addr_upper = 0x01;
	header.effects[0].stop_addr_lower = 0x23; //(9 * 4) - 1
	header.effects[0].repeat_count = 0x01;
	if (r < 0) { return r; }
	//Prepare waveform data
	Drv2667::drv2667_waveform_synthesis_t waveform[9];
	waveform[0].amplitude = 0xFF;
	waveform[0].frequency = 0x13;
	waveform[0].duration = 0x06;
	waveform[0].envelope = 0x00;

	waveform[1].amplitude = 0x00;
	waveform[1].frequency = 0x0D;
	waveform[1].duration = 0x05;
	waveform[1].envelope = 0x00;

	waveform[2].amplitude = 0x80;
	waveform[2].frequency = 0x1A;
	waveform[2].duration = 0x06;
	waveform[2].envelope = 0x00;

	waveform[3].amplitude = 0x00;
	waveform[3].frequency = 0x0D;
	waveform[3].duration = 0x05;
	waveform[3].envelope = 0x00;

	waveform[4].amplitude = 0xBF;
	waveform[4].frequency = 0x20;
	waveform[4].duration = 0x06;
	waveform[4].envelope = 0x00;

	waveform[5].amplitude = 0x00;
	waveform[5].frequency = 0x0D;
	waveform[5].duration = 0x05;
	waveform[5].envelope = 0x00;

	waveform[6].amplitude = 0xFF;
	waveform[6].frequency = 0x26;
	waveform[6].duration = 0x04;
	waveform[6].envelope = 0x00;

	waveform[7].amplitude = 0x00;
	waveform[7].frequency = 0x0D;
	waveform[7].duration = 0x05;
	waveform[7].envelope = 0x00;

	waveform[8].amplitude = 0xBF;
	waveform[8].frequency = 0x20;
	waveform[8].duration = 0x01;
	waveform[8].envelope = 0x08;

	//Write ram data
	r = drv.write_ram_data(header, waveform, 9);
	if (r < 0) { return r; }
	return 0;
}

int cook_parameterized_single(Drv2667 drv, uint8_t p_amplitude, uint8_t p_frequency, uint8_t p_duration, uint8_t p_envelope) {
	int r;
	//Prepare playlist
	uint8_t playlist[8];
	memset(playlist, 0x00, 8);
	playlist[0] = 0x01;
	r = drv.set_playlist(playlist);
	if (r < 0) { return r; }
	//Prepare header
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	//header.effects[0] = { start_addr_upper = ,
	header.effects[0].start_addr_upper = 0x81;
	header.effects[0].start_addr_lower = 0x00;
	header.effects[0].stop_addr_upper = 0x01;
	header.effects[0].stop_addr_lower = 0x03;
	header.effects[0].repeat_count = 0x01;
	if (r < 0) { return r; }
	//Prepare waveform data
	Drv2667::drv2667_waveform_synthesis_t waveform;
	waveform.amplitude = p_amplitude;
	waveform.frequency = p_frequency;
	waveform.duration = p_duration;
	waveform.envelope = p_envelope;
	//Write ram data
	r = drv.write_ram_data(header, &waveform, 1);
	if (r < 0) { return r; }

	return 0;
}

int cook_test_buzz(Drv2667 drv) {
	return cook_parameterized_single(drv, 0xFF, 0x19, 0x0A, 0x00);
}

void testDRV() {
	I2cmple* i2c = new I2cmple();
	Tca9548a tca = Tca9548a(i2c);
	tca.select(0x00);
	tca.select(0x02);

	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_100V_40_7DB);
	cook_test_buzz(drv);

	while (true) {
		printf("Press any key..\n");
		getchar();
		//Default init in digital mode
		drv.init_digital(Drv2667::DRV2667_TO_5MS);

		uint8_t buff;
		i2c->read8(0x59, 0x00, &buff);
		printf("Debug register:\n");
		print_bits(buff);
		i2c->read8(0x59, 0x02, &buff);
		printf("Control register:\n");
		print_bits(buff);

		printf("PLAYING...\n");
		drv.play();

		do {
			i2c->read8(0x59, 0x02, &buff);
		} while ((0x01 & buff) == 0x01);
	}
}

void testShortWrite() {
    I2cmple* i2c = new I2cmple();
	int r = 0;
	Tca9548a tca = Tca9548a(i2c);
	r = tca.select(0x00);
	if (r < 0) {perror("Error while short write");}
	r = tca.select(0x02);
	if (r < 0) {perror("Error while short write");}
}

void testBigSlider() {
    I2cmple* i2c = new I2cmple();
	int r = 0;
	Cap1214 slider = Cap1214(i2c);
	if (r < 0) {perror("Error during init");}
	r = slider.set_sensor_sensitivity(0b00000100);
	if (r < 0) {perror("Error during set sensitiv");}
	float pos;

	int size = 11;
	int8_t buff[size];
	for (int i = 0; i < size; i++) {
		buff[i] = 0;
	}

	while (true) {
        float buffc [2];
        buffc[0] = 0;
        buffc[1] = 0;
        r = slider.read_cursor_position(buffc, 100);
		r = slider.read_delta_count(buff);
		if (r < 0) {perror("Error during read position");}

		for (int i=0; i < size; i++) {
			printf("[%d]", buff[i]);
		}
		printf("\n");
        printf("pos %f max: %f\n", buffc[0],buffc[1]);
	}
}

void testSmallSlider() {
    I2cmple* i2c = new I2cmple();
	int r = 0;
	Cap1188 slider = Cap1188(i2c, Cap1188::CAP1188_ADDR_DEFAULT);
	if (r < 0) {perror("Error during init");}
	r = slider.enable_led_debug();
	if (r < 0) {perror("Error during enable led debug");}
	r = slider.set_sensor_sensitivity(0b00000100);
	if (r < 0) {perror("Error during set sensitiv");}
	float pos;

	while (true) {
		r = slider.read_cursor_position(&pos, 100);
		if (r < 0) {perror("Error during read position");}
		printf("%f\n", pos);
	}
}

void testButtonSet() {
	I2cmple* i2c = new I2cmple();
	Cap1188 button = Cap1188(i2c, Cap1188::CAP1188_ADDR_DEFAULT);
	Gpio_irq irq = Gpio_irq(Gpio_irq::GPIO_23, Gpio_irq::LOW, Gpio_irq::RISING);
	ButtonSet bs = ButtonSet(&button, &irq, ((uint8_t) 6));
	int8_t buffer[6];
	while(true) {
		bs.read_delta_count(buffer);
		for (int i=0; i < 6 ;i++) {
			printf("%d ", buffer[i]);
		}
		printf("\n");
	}
}

void testDRVAnalog() {
	I2cmple* i2c = new I2cmple();
	//Tca9548a tca = Tca9548a(i2c);
	//tca.select(0x00);
	//tca.select(0x02);
	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_50V_34_8DB);
	//Default init in analog mode
	drv.init_analog();
	while (true) {
		uint8_t buff;
		i2c->read8(0x59, 0x01, &buff);
		printf("Control register:\n");
		print_bits(buff);
		usleep(1000*200);
	}
}

/**
 * Returns the duration in ms of the given envelope
 * @param env The envelope to convert in ms.
 */
int get_synthesis_envelope_duration(Drv2667::drv2667_synthesis_envelope_t env) {
	if (env <= 8) {
		return env * 32;
	} else {
		return 256 + (env-8)*256;
	}
}

/**
 * amplitude: 	0->1
 * frequency: 	Hz
 * Ramp up:		0b0000 -> 0b1111
 * Ramp down:	0b0000 -> 0b1111
 */
Drv2667::drv2667_waveform_synthesis_t make_waveform(float amplitude, float frequency, Drv2667::drv2667_synthesis_envelope_t ramp_up, Drv2667::drv2667_synthesis_envelope_t ramp_down) {
	Drv2667::drv2667_waveform_synthesis_t waveform;
	waveform.amplitude	= (int)roundf(amplitude*255);
	waveform.frequency	= (int)roundf(frequency/7.8125);
	waveform.duration	= ((int)roundf((32*frequency)/1000))-get_synthesis_envelope_duration(ramp_down);//Fix bug here
	waveform.envelope	= (ramp_up << 4) || ramp_down;
	//TODO fix the duration thing
	return waveform;
}

int drv2667_cook_pushbutton(Drv2667 drv) {
	int r;
	//Prepare playlist
	//TODO, go back to this later
	uint8_t playlist[8];
	memset(playlist, 0x00, 8);
	playlist[0] = 0x01;
	r = drv.set_playlist(playlist);
	if (r < 0) { return r; }

	//Prepare header
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	header.effects[0].start_addr_upper	= 0b10000001;
	header.effects[0].start_addr_lower	= 0;
	header.effects[0].stop_addr_upper	= 0b00000001;
	header.effects[0].stop_addr_lower	= 11;
	header.effects[0].repeat_count = 0x01;
	if (r < 0) { return r; }

	//Prepare waveform data
	Drv2667::drv2667_waveform_synthesis_t waveform[4];
	//Prepare Jump Up Waveform
	//waveform[0] = make_waveform(0.75)
	waveform[0].amplitude = 0xBF;
	waveform[0].frequency = 0x34;
	waveform[0].duration = 0x01;
	waveform[0].envelope = 0x01;
	//Prepare Jump Down Waveform
	waveform[1].amplitude = 0xBF;
	waveform[1].frequency = 0x32;
	waveform[1].duration = 0x0D;
	waveform[1].envelope = 0x01;
	//Prepare Bottom Out waveform
	waveform[2].amplitude = 0x80;
	waveform[2].frequency = 0x1A;
	waveform[2].duration = 0x06;
	waveform[2].envelope = 0x00;

	waveform[3].amplitude = 0x80;
	waveform[3].frequency = 0x1A;
	waveform[3].duration = 0x06;
	waveform[3].envelope = 0x00;


	//Write ram data
	r = drv.write_ram_data(header, waveform, 9);
	if (r < 0) { return r; }
	return 0;
}

void testJumpUp() {
	I2cmple* i2c = new I2cmple();
	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_50V_34_8DB);
	drv.init_digital(Drv2667::DRV2667_TO_5MS);
	//cook_parameterized_single(drv, 0xBF, 0x34, 0x0A, 0x10);
	cook_parameterized_single(drv, 0xFF, 0x34, 0x01, 0x01);

	while (true) {
		printf("Press any key to play jump up..\n");
		getchar();

		uint8_t buff;
		printf("PLAYING...\n");
		drv.play();

		do {
			i2c->read8(0x59, 0x02, &buff);
		} while ((0x01 & buff) == 0x01);
	}
}

float pulse(float time) {
    const float pi = 3.14;
    const float frequency = 410; // Frequency in Hz
    return 0.5*(1+sin(2 * pi * frequency * time));
}

void testDrvFIFO() {
	I2cmple* i2c = new I2cmple();
	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_100V_40_7DB);
	drv.init_digital(Drv2667::DRV2667_TO_20MS);
	uint8_t buff;
	do {
		i2c->read8(0x59, 0x00, &buff);
		printf("Waiting for FIFO");
	} while ((0x01 & buff) == 0x01);
	printf("FIFO OP\n");
	while (true) {

		printf("Check FIFO state:\n");
		i2c->read8(0x59, 0x00, &buff);
		print_bits(buff);

		int duration = 100;
		uint8_t buffer[duration];
		//Calcul du temps step 1/8000
		for (int i=0; i < duration; i++) {
			buffer[i] = ((int)roundf(pulse(i*(1.0/8000.0))*255))-128;
			buffer[i] = 0x7F;
			//buffer[i] = ((int)roundf((pulse(i)-0.5)*255));
			//printf("%d ", i);
			//printf("%d\n", (int8_t)buffer[i]);
		}
		printf("Writing to the FIFO\n");
		i2c->write_bytes(0x59, Drv2667::FIFO, buffer, duration);
		usleep(1000000);
		i2c->write_bytes(0x59, Drv2667::FIFO, buffer, duration);
		usleep(1000000);
		i2c->write_bytes(0x59, Drv2667::FIFO, buffer, duration);


		/*
		do {
			i2c->read8(0x59, 0x00, &buff);
			printf("Oh wait\n");
		} while ((0x01 & buff) == 0x01);
		memset(buffer, 0x00, duration);
		i2c->write_bytes(0x59, Drv2667::FIFO, buffer, duration);
		do {
			i2c->read8(0x59, 0x00, &buff);
			printf("Oh wait\n");
		} while ((0x01 & buff) == 0x01);
		memset(buffer, 0x80, duration);
		i2c->write_bytes(0x59, Drv2667::FIFO, buffer, duration);
		do {
			i2c->read8(0x59, 0x00, &buff);
			printf("Oh wait\n");
		} while ((0x01 & buff) == 0x01);
		*/
		/*

		for () {


		for (int i=0; i < 255; i++) {

			i2c->write8(0x59, Drv2667::FIFO, i, 0xFF);
		}
		for (int i=0; i < 255; i++) {
			i2c->write8(0x59, Drv2667::FIFO, 255-i, 0xFF);
		}	*/
		printf("Check FIFO state:\n");
		i2c->read8(0x59, 0x00, &buff);
		print_bits(buff);


		printf("Press any key..\n");
		getchar();
		/*
		do {
			i2c->read8(0x59, 0x02, &buff);
		} while ((0x01 & buff) == 0x01);*/
	}
}

Drv2667::drv2667_header_t makeHeader(int length) {
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	header.effects[0].start_addr_upper	= 0b00000001;
	header.effects[0].start_addr_lower	= 0;
	header.effects[0].stop_addr_upper 	= ((int)floor((float)length/(float)255));
	header.effects[0].stop_addr_lower	= length%255;
	header.effects[0].repeat_count = 0x01;

	//DEBUG
	printf("------------------------------------------- \n");
	printf("stop upper: %d \n", header.effects[0].stop_addr_upper);
	printf("stop addr : %d \n", header.effects[0].stop_addr_lower);
	printf("repeatcoun: %d \n", header.effects[0].repeat_count);
	printf("------------------------------------------- \n");
	//DEBUG

	return header;
}

void testDirectPlayback() {
	I2cmple* i2c = new I2cmple();
	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_100V_40_7DB);
	drv.init_digital(Drv2667::DRV2667_TO_20MS);
	uint8_t buff;

	//Write to RAM
	//Prepare playlist
	uint8_t playlist[8];
	memset(playlist, 0x00, 8);
	playlist[0] = 0x01;
	drv.set_playlist(playlist);
	int duration = 240;

	//Prepare enveloppe
	float envbuffer[duration];
	for (int i=0; i < duration; i++) {
		envbuffer[duration - i] = (float)i/(float)duration;
	}

	//Prepare waveform
	int8_t buffer[duration];
	for (int i = 0; i < duration; i++) {
		buffer[i] = int8_t(roundf(pulse(i*(1.0/8000.0)*255))-128);
		//DEBUG
		if (i == 0) {
			printf("--------------------------\n");
			printf("%f \n", int(roundf(envbuffer[i])));
			printf("---------------------------\n");
		}
		//DEBUG
		printf("%d %d\n", i, buffer[i]);
	}

	//Prepare header
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	header.effects[0].start_addr_upper	= 0b00000001;
	header.effects[0].start_addr_lower	= 0;
	header.effects[0].stop_addr_upper 	= ((int)floor((float)duration/(float)255)) + 1;
	header.effects[0].stop_addr_lower	= duration%255;
	header.effects[0].repeat_count = 0x01;

	//DEBUG
	printf("------------------------------------------- \n");
	printf("stop upper: %d \n", header.effects[0].stop_addr_upper);
	printf("stop addr : %d \n", header.effects[0].stop_addr_lower);
	printf("repeatcoun: %d \n", header.effects[0].repeat_count);
	printf("------------------------------------------- \n");
	//DEBUG

	drv.switch_page(1);//Move to ram page 0
	//Set size  (N waveform (5N bytes) + 1 header size (1 byte))
    i2c->write8(DRV2667_ADDR, 0x00, 5 * header.neffect + 1, 0xFF);
	//Write array of effect
    i2c->write_bytes(DRV2667_ADDR, 0x01, (uint8_t *)header.effects,
							header.neffect * sizeof(Drv2667::drv2667_effect_t));
	//Write waveforms
	drv.switch_page(2);//Move to ram page 1
	i2c->write_bytes(DRV2667_ADDR, 0x00, (uint8_t *)buffer, duration);
	drv.switch_page(0);//Go back to control page

	while (true) {
		printf("Press any key..\n");
		getchar();
		drv.play();

		do {
			i2c->read8(0x59, 0x00, &buff);
		} while ((0x01 & buff) == 0x01);
	}
}

void testMultiDrv() {
	I2cmple* i2c = new I2cmple();
	Drv2667 drv = Drv2667(i2c, Drv2667::DRV2667_100V_40_7DB);
	Tca9548a tca = Tca9548a(i2c);
	tca.select(0x00);
	tca.select(0x01);
	drv.init_digital(Drv2667::DRV2667_TO_20MS);
	tca.select(0x02);
	drv.init_digital(Drv2667::DRV2667_TO_20MS);
	uint8_t buff;

	//Write to RAM
	//Prepare playlist
	uint8_t playlist[8];
	memset(playlist, 0x00, 8);
	playlist[0] = 0x01;
	tca.select(0x01);
	drv.set_playlist(playlist);
	tca.select(0x02);
	drv.set_playlist(playlist);
	int duration = 240;

	//Prepare enveloppe
	float envbuffer[duration];
	for (int i=0; i < duration; i++) {
		envbuffer[duration - i] = (float)i/(float)duration;
	}

	//Prepare waveform
	int8_t buffer[duration];
	for (int i = 0; i < duration; i++) {
		buffer[i] = int8_t(roundf(pulse(i*(1.0/8000.0)*255))-128);
		//DEBUG
		if (i == 0) {
			printf("--------------------------\n");
			printf("%f \n", int(roundf(envbuffer[i])));
			printf("---------------------------\n");
		}
		//DEBUG
		printf("%d %d\n", i, buffer[i]);
	}

	//Prepare header
	Drv2667::drv2667_header_t header;
	Drv2667::drv2667_effect_t effects[1];
	header.neffect = 1;
	header.effects = effects;
	header.effects[0].start_addr_upper	= 0b00000001;
	header.effects[0].start_addr_lower	= 0;
	header.effects[0].stop_addr_upper 	= ((int)floor((float)duration/(float)255)) + 1;
	header.effects[0].stop_addr_lower	= duration%255;
	header.effects[0].repeat_count = 0x01;

	//DEBUG
	printf("------------------------------------------- \n");
	printf("stop upper: %d \n", header.effects[0].stop_addr_upper);
	printf("stop addr : %d \n", header.effects[0].stop_addr_lower);
	printf("repeatcoun: %d \n", header.effects[0].repeat_count);
	printf("------------------------------------------- \n");
	//DEBUG

	tca.select(0x01);
	drv.switch_page(1);//Move to ram page 0
	//Set size  (N waveform (5N bytes) + 1 header size (1 byte))
    i2c->write8(DRV2667_ADDR, 0x00, 5 * header.neffect + 1, 0xFF);
	//Write array of effect
    i2c->write_bytes(DRV2667_ADDR, 0x01, (uint8_t *)header.effects,
							header.neffect * sizeof(Drv2667::drv2667_effect_t));
	//Write waveforms
	drv.switch_page(2);//Move to ram page 1
	i2c->write_bytes(DRV2667_ADDR, 0x00, (uint8_t *)buffer, duration);
	drv.switch_page(0);//Go back to control page
	//Same thing for drv n°2
	tca.select(0x02);
	drv.switch_page(1);//Move to ram page 0
	//Set size  (N waveform (5N bytes) + 1 header size (1 byte))
    i2c->write8(DRV2667_ADDR, 0x00, 5 * header.neffect + 1, 0xFF);
	//Write array of effect
    i2c->write_bytes(DRV2667_ADDR, 0x01, (uint8_t *)header.effects,
							header.neffect * sizeof(Drv2667::drv2667_effect_t));
	//Write waveforms
	drv.switch_page(2);//Move to ram page 1
	i2c->write_bytes(DRV2667_ADDR, 0x00, (uint8_t *)buffer, duration);
	drv.switch_page(0);//Go back to control page

	while (true) {
		printf("Press any key..\n");
		getchar();
		tca.select(0x02);
		//drv.play();
		//tca.select(0x02);
		drv.play();

		do {
			i2c->read8(0x59, 0x00, &buff);
		} while ((0x01 & buff) == 0x01);
	}
}

void testTrackPad() {
	I2cmple* i2c = new I2cmple();
	Mtch6303 mtch = Mtch6303(i2c);

	Mtch6303::mtch6303_touch_data_t touch[10];
	uint8_t nbTouch;
	int loop = 1000;
	for (int i=0; i < loop; i++) {
		mtch.read_touch_data(&nbTouch, touch);
		printf("Nbtouch: %d\n", nbTouch);
		for (int i=0; i < nbTouch; i++) {
			printf("----------------------------------------\n");
			printf("touchId: %d\n", touch[i].touchId);
			printf("x %d\n", touch[i].x);
			printf("y %d\n", touch[i].y);
		}
		printf("Press any key\n");
		getchar();
	}
}

int main(int argc, char* argv[]) {
	//testButtonSet();
	//testShortWrite();
	//testBigSlider();
	//testInterrupt();
	//testDRV();
	//testSmallSlider();
	//testInterrupt2();
	//testDRVAnalog();
	//testDrvFIFO();
	//testDirectPlayback();
	//testMultiDrv();
	testTrackPad();

	return 0;
}



