/**
 * @file i2cmple.cpp
 * @authors Julien Decaudin
 * @brief I2C communication on Raspbian
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "i2cmple.h"
//#ifdef linux
extern "C" {
#include "include/linux/i2c-dev.h"
}
//#endif
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <stdio.h>

I2cmple::I2cmple() {
	#ifdef linux
	unsigned long func;
	//Open file descriptor in /dev/i2c-1
	this->fd = open("/dev/i2c-1", O_RDWR);
	//TODO check for error
	//if (fd < 0) { perror("Can't open i2c file"); }
	//Test for I2C support
	if (ioctl(this->fd, I2C_FUNCS, &func) < 0) {
		if ((func & I2C_FUNC_I2C) == 0) {
			//perror("I2C not supported");
			//TODO check for error
		}
	}
	#endif
}

I2cmple::~I2cmple() {
	if (close(this->fd)) {
		//perror("");
		//TODO check for error
	}
}

int I2cmple::short_read(uint8_t addr, uint8_t *data) {
	int file;
	char filename[20];

	sprintf(filename, "/dev/i2c-%d", 1);
	file = open(filename, O_RDWR);
	if (file < 0) {printf("Error: Could not open file\n"); return -1;}

	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		printf("Error: Could not set slave adress\n");
		return -1;
	}

	*data = i2c_smbus_read_byte(file);

	close(file);
	if (*data < 0) {
		printf("Error: Read failed\n");
		return -1;
	}

	return 0;
}

int I2cmple::read8(uint8_t addr, uint8_t reg, uint8_t *data) {
	return read_bytes(addr, reg, data, 1);
}

int I2cmple::read_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb) {
	int r = -1;
	#ifdef linux
	i2c_rdwr_ioctl_data message_queue;
	i2c_msg messages[1];
	//Prepare ioctl_data struct
	message_queue.nmsgs = 1;
	message_queue.msgs = messages;
	//Write register address message
	messages[0].addr = addr;
	messages[0].len = 1;
	messages[0].flags = 0;
	messages[0].buf = &reg;
	r = ioctl(this->fd, I2C_RDWR, &message_queue);
	if (r < 0) { return r; }
	//Prepare read message
	messages[0].addr = addr;
	messages[0].len = nb;
	messages[0].flags = I2C_M_RD;
	messages[0].buf = data;
	r = ioctl(this->fd, I2C_RDWR, &message_queue);
	if (r < 0) { return r; }
	#endif
	return 0;
}

int I2cmple::short_write(uint8_t addr, uint8_t data) {
	int r = -1;
    #ifdef linux
	uint8_t buff[0];
	i2c_rdwr_ioctl_data message_queue;
	i2c_msg messages[1];
	//Prepare rdwr_ioctl_data struct
	message_queue.nmsgs = 1;
	message_queue.msgs = messages;
	//Fill buffer
	buff[0] = data;
	//Write register
	messages[0].addr = addr;
	messages[0].len = 1;
	messages[0].flags = 0;
	messages[0].buf = buff;
	r = ioctl(this->fd, I2C_RDWR, &message_queue);
    #endif
	return r;
}

int I2cmple::write8(uint8_t addr, uint8_t reg, uint8_t data, uint8_t mask) {
	int r = -1;
	#ifdef linux
	uint8_t buff[2];
	i2c_rdwr_ioctl_data message_queue;
	i2c_msg messages[1];
	//Prepare rdwr_ioctl_data struct
	message_queue.nmsgs = 1;
	message_queue.msgs = messages;
	//Fill buffer
	buff[0] = reg;
	buff[1] = 0x00;
	if (mask != 0xFF) {
		//Read old register value
		r = read8(addr, reg, &buff[1]);
		if (r < 0) { return r;	}
	}
	//Fill buffer according to mask
	buff[1] = (buff[1] & ~mask) | (data & mask);

	//Write register
	messages[0].addr = addr;
	messages[0].len = 2;
	messages[0].flags = 0;
	messages[0].buf = buff;
	r = ioctl(this->fd, I2C_RDWR, &message_queue);
	#endif
	return r;
}

int I2cmple::write_bytes(uint8_t addr, uint8_t reg, uint8_t *data, int nb) {
	int r = -1;
	#ifdef linux
	uint8_t buff[1+nb];
	i2c_rdwr_ioctl_data message_queue;
	i2c_msg messages[1];
	//Prepare rdwr_ioctl_data struct
	message_queue.nmsgs = 1;
	message_queue.msgs = messages;
	//Fill buffer
	buff[0] = reg;
	memcpy(&buff[1], data, nb);
	//Write register
	messages[0].addr = addr;
	messages[0].len = 1 + nb;
	messages[0].flags = 0;
	messages[0].buf = buff;
	r = ioctl(this->fd, I2C_RDWR, &message_queue);
	#endif
	return r;
}
