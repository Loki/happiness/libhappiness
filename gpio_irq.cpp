/**
 * @file gpio_irq.cpp
 * @authors Julien Decaudin
 * @brief handler for GPIO IRQ
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "gpio_irq.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <poll.h>


Gpio_irq::Gpio_irq(gpio_pin_t pin, gpio_active_t active, gpio_edge_t edge) : pin(pin), active(active), edge(edge) {
	int fd, r, len;
	char buf[64];
	//Test for export path
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd < 0) { perror("Error during export: Can't open file"); }
	//Path	& pin
	r = snprintf(this->path, sizeof(this->path), "/sys/class/gpio/gpio%d/", pin);
	if (r < 0) { perror("Building path"); }
	//Test for "GPIO already exported"
	if (access(this->path, F_OK) == -1) {
		//Export
		len = snprintf(buf, sizeof(buf), "%d", pin);
		r = write(fd, buf, len);
		if (r < 0) { perror("Error during export"); }
    }
    r = close(fd);
    if (r < 0) { perror("Error during export: Can't close file");}
    //Set direction	to in
    fd = open(strcat(strcpy(buf, this->path), "direction"), O_WRONLY);
    if (fd < 0) { perror("Open [...]/direction"); }
    r = write(fd, "in", 2);
    if (r < 0) { perror("Write [...]/direction"); }
    r = close(fd);
    if (r < 0) { perror("Close [...]/direction"); }
	//Set active (to 1)
	fd = open(strcat(strcpy(buf, this->path), "active_low"), O_WRONLY);
	if (fd < 0) { perror("Open [...]/active_low"); }
	r = write(fd, ((active == LOW) ? "1":"0"), 1);
	if (r < 0) { perror("Write [...]/active_low"); }
	r = close(fd);
	if (fd < 0) { perror("Close [...]/active_low"); }
	//Set edge (to rising)
	fd = open(strcat(strcpy(buf, this->path), "edge"), O_WRONLY);
	if (fd < 0) { perror("Open [...]/edge");}
	r = write(fd, ((edge == RISING) ? "rising":"falling"), ((edge == RISING) ? 6:7));
	if (r < 0) { perror("Write [...]/edge"); }
	r = close(fd);
	if (r < 0) { perror("Close [...]/edge"); }
}

int Gpio_irq::unexport() {
	int fd, r, len;
	char buf[64];
	//Test for export path
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd < 0) { perror("Error during unexport: Can't open file"); }
	//Export
	len = snprintf(buf, sizeof(buf), "%d", this->pin);
	r = write(fd, buf, len);
	if (r < 0) { perror("Error during unexport"); }
	r = close(fd);
	if (r < 0) { perror("Error during unexport: Can't close file"); }
	return r;
}

void Gpio_irq::waitForIRQ() {
	//TODO: Fix bug occuring when waiting for IRQ when no wire is plugged
	int fd;
	int r = 0;
	int nfds = 1;//Number of file descriptor
	int timeout = -1;
	struct pollfd fdset[nfds];//Struct for poll
	char buf[64];

	fd = open(strcat(strcpy(buf, this->path), "value"), O_RDONLY | O_NONBLOCK );
	if (r < 0) { perror("Open [...]/value"); }

	fdset[0].fd = fd;
	fdset[0].events = POLLPRI;
	fdset[0].revents = 0;
	//Consome prior interrupt
	lseek(fdset[0].fd, 0, SEEK_SET);
	read(fdset[0].fd, buf, 64);

	//Start polling
	r = poll(fdset, nfds, timeout);
	if (r < 0) { perror("poll() failed!\n"); }
	if (fdset[0].revents & POLLPRI) {
		lseek(fdset[0].fd, 0, SEEK_SET);
		read(fdset[0].fd, buf, 64);
	} else {
		perror("?");
	}
	r = close(fd);
	if (r < 0) { perror("Close [...]/value"); }
}
