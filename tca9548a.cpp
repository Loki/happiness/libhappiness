/**
 * @file tca9548a.cpp
 * @authors Julien Decaudin
 * @brief tca9548a control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "tca9548a.h"

Tca9548a::Tca9548a(I2C* i2cH) : i2c(i2cH) {}

int Tca9548a::select(uint8_t channels) {
    return i2c->short_write(TCA9548A_ADDR, channels);
}

