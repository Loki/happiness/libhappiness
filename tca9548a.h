/**
 * @file tca9548a.h
 * @authors Julien Decaudin
 * @brief tca9548a control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once

#include "i2cmple.h"

#define TCA9548A_ADDR	0x70

class Tca9548a {
	public:
        Tca9548a(I2C* i2cH);
		int select(uint8_t channels);
	private:
        I2C* i2c;	/**< I2C Communication*/
};
