/**
 * @file mtch6303.cpp
 * @authors Julien Decaudin
 * @brief MTCH6303 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "mtch6303.h"


Mtch6303::Mtch6303(I2C* i2cH) : i2c(i2cH) {}

int Mtch6303::read_touch_data(uint8_t *nbTouch, mtch6303_touch_data_t *buff) {
	int r;
	//Read n° of touch
	r = i2c->short_write(MTCH6303_ADDR, 0x00);
	r = i2c->short_read(MTCH6303_ADDR, nbTouch);

	if (*nbTouch > 10) {
		*nbTouch = 0;
	} else {
		//mtch6303_touch_data_t touch[10];
		r = i2c->read_bytes(MTCH6303_ADDR, MTCH6303_TOUCH_0, (uint8_t*)buff, 6 * (*nbTouch));
		if (r < 0) { printf("Error while reading");	}
	}

	return r;
}
