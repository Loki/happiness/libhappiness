/**
 * @file button_set.h
 * @authors Julien Decaudin
 * @brief Button handler for cap1188
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once
#include "cap1188.h"
#include "gpio_irq.h"

class ButtonSet {
	public:
		/**
		 * Init the ButtonSet
		 * @param cap The cap structure that handle all of the button in this set
		 * @param irq The irq handler for the given button set
		 * @param nbBtn The number of button in this button set
		 */
		ButtonSet(Cap1188* cap, Gpio_irq* irq, uint8_t nbBtn);

		int read_delta_count(int8_t* buffer);

		int set_sensor_threshold(int8_t* buffer);

		int set_polling_threshold(int8_t* buffer);

		uint8_t is_polling();

	private:
		uint8_t keepPolling;
		Cap1188* cap;
		Gpio_irq* irq;
		uint8_t nbBtn;
		int8_t threshold[8];
};

