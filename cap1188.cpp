/**
 * @file cap1188.cpp
 * @authors Julien Decaudin
 * @brief CAP1188 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "cap1188.h"

Cap1188::Cap1188(I2C* i2cH, cap1188_addr_t addr): i2c(i2cH) {
	this->addr = addr;
}

int Cap1188::enable_led_debug() {
    return i2c->write8(this->addr, CAP1188_LED_DEBUG, 0xFF, 0xFF);
}

int Cap1188::set_sensor_threshold(int8_t *list) {
	return i2c->write_bytes(this->addr, CAP1188_SENSOR_THRES, (uint8_t*)list, 8);
}

int Cap1188::set_sensor_sensitivity(uint8_t sensitivity) {
    return i2c->write8(this->addr, CAP1188_SENS_CTRL, sensitivity << 4, 0b01110000);
}

int Cap1188::clear_interrupt_bit() {
    return i2c->write8(this->addr, CAP1188_MAIN_CTRL, 0x00, 0b00000001);
}

int Cap1188::recalibrate_sensors() {
	int r;
	r = i2c->write8(this->addr, CAP1188_FRCE_CALIB, 0xFF, 0xFF);
	if (r < 0) { return r; }
	uint8_t res = 0xFF;
	while ((res = i2c->read8(this->addr, CAP1188_FRCE_CALIB, &res)) != 0x00) {}
	return r;
}

int Cap1188::set_interrupt_repeat(uint8_t value) {
    return i2c->write8(this->addr, CAP1188_REPEAT, value, 0xFF);
}

int Cap1188::read_sensor_threshold(int8_t *buffer) {
	return i2c->read_bytes(this->addr, CAP1188_SENSOR_THRES, (uint8_t*)buffer, 8);
}

int Cap1188::read_sensors_satus(uint8_t *buffer) {
	int r;
    r = i2c->read8(this->addr, CAP1188_SENSOR_STAT, buffer);
	if (r < 0) { return r; }
	r = clear_interrupt_bit();
	return r;
}

int Cap1188::read_delta_count(int8_t *buffer) {
	int r;
	uint8_t buff[8];
    r = i2c->read_bytes(this->addr, CAP1188_DELTA_COUNT, buff, 8);
	if (r < 0) { return r; }
	for (int i=0; i < 8; i++) {
		buffer[i] = (buff[i] ^ 0x80) - 0x80;
	}
	return r;
}

int Cap1188::read_cursor_position(float *buffer, int resolution) {
	int r;
	int nbElectrode = 4;
	int8_t S_befor, S_after, S;
	int8_t buff[8];
	r = read_delta_count(buff);
	if (r < 0) { return r; }

	int8_t pos, max;
	max = -1;
	pos = -1;
	for (int i=0; i < nbElectrode;i++) {
		//Clear negative value
		if (buff[i] < 0) {
			buff[i] = 0;
		}
		if (max < buff[i]) {
			pos = i;
			max = buff[i];
		}
	}

	if (max == 0) {
		*buffer = 0.0;
		return r;
	} else if (pos==0) {//First electrode
		S_befor = 0;
		S_after = buff[1];
	} else if (pos == (nbElectrode - 1)) {//Last electrode
		S_befor = buff[pos-1];
		S_after = 0;
	} else {
		S_befor = buff[pos-1];
		S_after = buff[pos+1];
	}

	S = buff[pos];
	*buffer = ((float)((float)(S_after-S_befor)/(S+S_after+S_befor)+pos) * (float)resolution/(nbElectrode - 1));
	return r;
}
