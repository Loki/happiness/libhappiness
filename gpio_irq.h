/**
 * @file gpio_irq.h
 * @authors Julien Decaudin
 * @brief handler for GPIO IRQ
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

class Gpio_irq {
	public:
		enum gpio_pin_t {
			GPIO_2	= 2,
			GPIO_3	= 3,
			GPIO_4	= 4,
			GPIO_5	= 5,
			GPIO_6	= 6,
			GPIO_7	= 7,
			GPIO_8	= 8,
			GPIO_9 	= 9,
			GPIO_10 = 10,
			GPIO_11	= 11,
			GPIO_12	= 12,
			GPIO_13	= 13,
			GPIO_16	= 16,
			GPIO_17	= 17,
			GPIO_18	= 18,
			GPIO_19	= 19,
			GPIO_20	= 20,
			GPIO_21 = 21,
			GPIO_22	= 22,
			GPIO_23 = 23,
			GPIO_24 = 24,
			GPIO_25	= 25,
			GPIO_26	= 26,
			GPIO_27	= 27
		};

		enum gpio_active_t {
			HIGH = 0,
			LOW = 1
		};

		enum gpio_edge_t {
			RISING,
			FALLING
		};

		/**
		 * Init the GPIO pin to enable IRQ handling.
		 * @param pin The pin on which the IRQ is expected.
		 * @param active Specify if the IRQ is Active_low or Active_high.
		 * @param edge Specify if the IRQ happen on rising or falling edge.
		 */
		Gpio_irq(gpio_pin_t pin, gpio_active_t active, gpio_edge_t edge);

		/**
		 * Unexport the GPIO.
		 * @param irq Ptr to the irq struct
		 */
		int unexport();

		/**
		 * Wait for an IRQ to occur.
		 * @param irq Ptr to the irq struct
		 */
		void waitForIRQ();

	private:
		gpio_pin_t pin;			/**< The pin on which the IRQ is expected. */
		gpio_active_t active;	/**< Specify if the IRQ is Active_low or Active_high. */
		gpio_edge_t edge;		/**< Specify if the IRQ happen on rising or falling edge. */
		char path[64];
};
