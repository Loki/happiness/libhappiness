/**
 * @file button_set.cpp
 * @authors Julien Decaudin
 * @brief Button Handler
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include "button_set.h"
#include <stdio.h>

ButtonSet::ButtonSet(Cap1188* p_cap, Gpio_irq* p_irq, uint8_t nbBtn) : cap(p_cap), irq(p_irq) {
	this->nbBtn = nbBtn;
	this->keepPolling = 0;
	cap->read_sensor_threshold(threshold);
}

int ButtonSet::read_delta_count(int8_t* buffer) {
	if (keepPolling == 0) {
		cap->clear_interrupt_bit();
		irq->waitForIRQ();
		keepPolling = 1;
	}
	cap->read_delta_count(buffer);
	keepPolling = 0;
	for (int i=0; i < nbBtn && keepPolling == 0; i++) {
		if (buffer[i] > threshold[i]) {
			keepPolling = 1;
		}
	}

	return 0;
}

int ButtonSet::set_sensor_threshold(int8_t *buffer) {
	cap->set_sensor_threshold(buffer);
}

int ButtonSet::set_polling_threshold(int8_t* buffer) {
	for (int i=0; i < nbBtn; i++) {
		threshold[i] = buffer[i];
	}
}

uint8_t ButtonSet::is_polling() {
	return keepPolling;
}
