/**
 * @file cap1188.h
 * @authors Julien Decaudin
 * @brief CAP1188 control
 * @copyright © Inria
 *
 * This file is part of libhappiness.
 *
 * libhappiness is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libhappiness is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libhappiness.  If not, see <https://www.gnu.org/licenses/>.
 **/

#pragma once
#include <stdint.h>
#include "i2cmple.h"

class Cap1188 {
	public:
		/**
		* CAP1188 available slaves address.
		*
		*/
		enum cap1188_addr_t {
			CAP1188_ADDR_3V			= 0x28,
			CAP1188_ADDR_DEFAULT	= 0x29,
			CAP1188_ADDR_600K		= 0x2A,
			CAP1188_ADDR_300K		= 0x2B,
			CAP1188_ADDR_180K		= 0x2C
		};

		enum cap1188_register_t {
			CAP1188_MAIN_CTRL		= 0x00,/**< Main Control.*/
			CAP1188_GEN_STATUS		= 0x02,
			CAP1188_SENSOR_STAT		= 0x03,/**< Sensor Input Status. Returns the state of the sampled capacitive touch sensor inputs.*/
			CAP1188_LED_STATUS		= 0x04,
			CAP1188_DELTA_COUNT		= 0x10,/**< [0x10 -> 0x17] Sensor Input Delta Count. Stores the delta count for CSx (0x10 = CS1, 0x17 = CS8)*/
			CAP1188_REPEAT			= 0x28,/**< Repeat Rate Enable Enables repeat rate for all sensor inputs*/
			CAP1188_SENSOR_THRES	= 0x30,/**< [30h -> 37h] Sensor Input Threshold. Stores the delta count threshold to determine a touch for Capacitive Touch Sensor Input x (30h = 1, 37h = 8)*/
			CAP1188_CONFIG_2		= 0x40,/**< Configuration 2 Stores additional configuration controls for the device .*/
			CAP1188_LED_DEBUG		= 0x72,/**< Sensor Input LED Linking. Controls linking of sensor inputs to LED channels */
			CAP1188_SENS_CTRL		= 0x1F,/**<	Sensitivity Control. Controls the sensitivity of the threshold and delta counts and data scaling of the base counts */
			CAP1188_FRCE_CALIB		= 0x26 /**< Calibration Activate Forces re-calibration for capacitive touch sensor inputs. */
		/*
		0Ah R 				Noise Flag Status Stores the noise flags for sensor inputs 00h Page 34
		20h R/W 			Configuration Controls general functionality 20h Page 37
		21h R/W 			Sensor Input Enable Controls whether the capacitive touch sensor inputs are sampled
		22h R/W 			Sensor Input Configuration Controls max duration and auto-repeat delay for sensor inputs operating in the full power state
		23h R/W 			Sensor Input Configuration 2 Controls the MPRESS controls for all sensor inputs
		24h R/W 			Averaging and Sampling Config Controls averaging and sampling window

		27h R/W 			Interrupt Enable Enables Interrupts associated with capacitive touch sensor inputs

		2Ah R/W 			Multiple Touch Configuration Determines the number of simultaneous touches to flag a multiple touch condition
		2Bh R/W 			Multiple Touch Pattern Configuration Determines the multiple touch pattern (MTP) configuration
		2Dh R/W 			Multiple Touch Pattern Determines the pattern or number of sensor inputs used by the MTP circuitry
		2Fh R/W 			Recalibration Configuration Determines re-calibration timing and sampling window
		38h R/W 			Sensor Input Noise Threshold Stores controls for selecting the noise threshold for all sensor inputs
		* Standby Configuration Registers
		40h R/W 			Standby Channel Controls which sensor inputs are enabled while in standby
		41h R/W 			Standby Configuration Controls averaging and cycle time while in standby
		42h R/W 			Standby Sensitivity Controls sensitivity settings used while in standby
		43h R/W 			Standby Threshold Stores the touch detection threshold for active sensor inputs in standby
		* Base Count Registers
		[50h -> 57h] R 		Sensor Input Base Count. Stores the reference count value for sensor input x (50h = 1, 57h = 8)
		* LED Controls
		71h R/W 			LED Output Type Controls the output type for the LED outputs

		73h R/W 			LED Polarity Controls the output polarity of LEDs
		74h R/W 			LED Output Control Controls the output state of the LEDs
		77h R/W 			Linked LED
		* Transition Control Controls the transition when LEDs are linked to CS channels
		79h R/W 			LED Mirror Control Controls the mirroring of duty cycles for the LEDs
		81h R/W 			LED Behavior 1 Controls the behavior and response of LEDs 1 - 4
		82h R/W 			LED Behavior 2 Controls the behavior and response of LEDs 5 - 8
		84h R/W 			LED Pulse 1 Period Controls the period of each breathe during a pulse
		85h R/W 			LED Pulse 2 Period Controls the period of the breathing during breathe and pulse operation
		86h R/W 			LED Breathe Period Controls the period of an LED breathe operation
		88h R/W 			LED Config Controls LED configuration
		90h R/W 			LED Pulse 1 Duty Cycle Determines the min and max duty cycle for the pulse operation
		91h R/W 			LED Pulse 2 Duty Cycle Determines the min and max duty cycle for breathe and pulse operation
		92h R/W 			LED Breathe Duty Cycle Determines the min and max duty cycle for the breathe operation
		93h R/W 			LED Direct Duty Cycle Determines the min and max duty cycle for Direct mode LED operation
		94h R/W 			LED Direct Ramp Rates Determines the rising and falling edge ramp rates of the LEDs
		95h R/W 			LED Off Delay Determines the off delay for all LED behaviors
		[B1h -> B8h] R 		Sensor Input Calibration Stores the upper 8-bit calibration value for sensor input x (B1h = 1, B8h = 8)
		B9h R 				Sensor Input Calibration LSB 1 Stores the 2 LSBs of the calibration value for sensor inputs 1 - 4
		BAh R 				Sensor Input Calibration LSB 2 Stores the 2 LSBs of the calibration value for sensor inputs 5 - 8
		*/
		};

		/**
		* Init the cap1188 to use with I2C
		* @param addr I2C address of the cap1188.
		*/
        Cap1188(I2C* i2cH, cap1188_addr_t addr);

		/**
		* Clear the interrupt bit from the main control register
		*/
		int clear_interrupt_bit();

		/**
		 * Run the recalibration routine
		 */
		int recalibrate_sensors();

		/**
		* Set the repeat of the interrupt when touch is detected
		* @param value 0xFF for enabled, 0x00 for disabled
		*/
		int set_interrupt_repeat(uint8_t value);

		/**
		* Read the sensor status register.
		* @param buffer to store the 8bit returned value
		*/
		int read_sensors_satus(uint8_t *buffer);

		/**
		 * Read the current threshold settings.
		 * @param buffer Ptr to a uint8_t array (size 8)
		 * @see set_sensor_threshold
		 */
		int read_sensor_threshold(int8_t *buffer);

		/**
		* Enable the led debug.
		* Light up the leds according to the sensors being activated
		*/
		int enable_led_debug();

		/**
		* Set the threshold value of all 8 sensors.
		* Specify the delta count value above which a touch is detected
		* @param list Ptr to a list of 8 values representing the threshold.
		* @note every threshold value should be in the [0:127] range
		*/
		int set_sensor_threshold(int8_t *list);

		/**
		* Set the sensitivity for all 8 sensors.
		* Specify the sensitivity with a value between 0b000 (most sensitive) and 0b111 (less sensitive)
		*/
		int set_sensor_sensitivity(uint8_t sensitivity);

		/**
		* Read the sensors input delta count.
		* Values range from -128 to 127
		* @param buffer Ptr to a int8_t array (size 8)
		*/
		int read_delta_count(int8_t *buffer);

		/**
		* Read the estimated position of the finger on the slider.
		* @note for slider only
		* @param buffer Ptr to a float
		* @param resolution Resolution wanted for the slider
		*/
		int read_cursor_position(float *buffer, int resolution);
	private:
        I2C* i2c;	/**<I2C communication.*/
		cap1188_addr_t addr;
};
